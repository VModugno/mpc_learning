
%% Initialize the "Cart-Pole" environment
%env = gym.make('CartPole-v0')

%% Defining the environment related constants

% Number of discrete states (bucket) per state dimension
NUM_BUCKETS = [1, 1, 6, 3];  % (x, x', theta, theta')
% Number of discrete actions
NUM_ACTIONS = 2; % (left, right)
% Bounds for each discrete state
%STATE_BOUNDS = list(zip(env.observation_space.low, env.observation_space.high))
STATE_BOUNDS(1) = [-0.5, 0.5];
STATE_BOUNDS(2) = [-100,100];
STATE_BOUNDS(3) = [-degtorad(50), degtorad(50)];
STATE_BOUNDS(4) = [-100,+100];
%Index of the action
%ACTION_INDEX = length(NUM_BUCKETS);



% Learning related constants
MIN_EXPLORE_RATE = 0.01;
MIN_LEARNING_RATE = 0.1;

% Defining the simulation related constants
NUM_EPISODES = 1000;
MAX_T = 250;
STREAK_TO_END = 120;
SOLVED_T = 199;
DEBUG_MODE = True;

function simulate(NUM_BUCKETS,NUM_ACTIONS,STATE_BOUNDS)
    % Creating a Q-Table for each state-action pair
    q_table = zeros(sum(NUM_BUCKETS),NUM_ACTIONS);
    % Instantiating the learning related parameters
    learning_rate = get_learning_rate(0,MIN_LEARNING_RATE);
    explore_rate = get_explore_rate(0,MIN_EXPLORE_RATE);
    discount_factor = 0.99;  % since the world is unchanging

    num_streaks = 0;

    for episode=1:NUM_EPISODES

        % Reset the environment (reset return the first state)
        init_state = env.reset();  %to fix

        % the initial state
        state_0 = state_to_bucket(init_state,STATE_BOUNDS);

        for t=1:MAX_T
            
            %env.render() to fix

            % Select an action 
            action = select_action(state_0,q_table,explore_rate);

            % Execute the action (step return the new state and the reward)
            [cur_state, reward, done] = env.step(action); % to fix

            % Observe the result
            state = state_to_bucket(cur_state,STATE_BOUNDS);

            % Update the Q based on the result
            best_q = max(q_table(state)); % max in row for the current state
            q_table(state_0,(action)) = q_table(state_0,(action)) + learning_rate*(reward + discount_factor*(best_q) - q_table(state_0,action));

            % Setting up for the next iteration
            state_0 = state;

            % Print data
            if (DEBUG_MODE)
                fprintf("\nEpisode = %d", episode);
                fprintf("t = %d", t);
                fprintf("Action: %d" , action);
                fprintf("State: %s" , str(state));
                fprintf("Reward: %f", reward);
                fprintf("Best Q: %f", best_q);
                fprintf("Explore rate: %f", explore_rate);
                fprintf("Learning rate: %f", learning_rate);
                fprintf("Streaks: %d", num_streaks);

                fprintf("")
            end

            if (done)
               fprintf("Episode %d finished after %f time steps" , episode, t);
               if (t >= SOLVED_T)
                   num_streaks = num_streaks + 1;
               else
                   num_streaks = 0;
               break
               end
            end

            %sleep(0.25)
        end
        % It's considered done when it's solved over 120 times consecutively
        if num_streaks > STREAK_TO_END
            break
        end

        % Update parameters
        explore_rate = get_explore_rate(episode,MIN_EXPLORE_RATE);
        learning_rate = get_learning_rate(episode,MIN_LEARNING_RATE);
    end
end

    function action = select_action(state,q_table,explore_rate)
        %Select a random action
        if random.random() < explore_rate
            action = env.action_space.sample();
        % Select the action with the highest q
        else
            [dummy, action] = max(q_table(state));
        end
    end

    function ret = get_explore_rate(t,MIN_EXPLORE_RATE)
        ret = max(MIN_EXPLORE_RATE, min(1, 1.0 - log10((t+1)/25)));
    end

    function ret = get_learning_rate(t,MIN_LEARNING_RATE)
        ret = max(MIN_LEARNING_RATE, min(0.5, 1.0 - log10((t+1)/25)));
    end

    function [bucket_indice] = state_to_bucket(state,STATE_BOUNDS)
        bucket_indice = zeros(1,length(STATE_BOUNDS));
        for i=1:length(state)
            if state(i) <= STATE_BOUNDS(i,0)
                bucket_index = 1;
            elseif state(i) >= STATE_BOUNDS(i,1)
                bucket_index = NUM_BUCKETS(i);
            else
                % Mapping the state bounds to the bucket array
                bound_width = STATE_BOUNDS(i,1) - STATE_BOUNDS(i,0);
                offset = (NUM_BUCKETS(i)-1)*STATE_BOUNDS(i,0)/bound_width;
                scaling = (NUM_BUCKETS(i)-1)/bound_width;
                bucket_index = int(round(scaling*state(i) - offset));
            end
            bucket_indice(1,i) = bucket_index;
        end
        %return  bucket_indice
    end
        

%if __name__ == "__main__":
%simulate()