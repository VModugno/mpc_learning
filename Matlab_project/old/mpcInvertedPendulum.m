clear; clc; close;

% Double integrator
A_cont = [0 1; 1 0];
B_cont = [0; 1];
C_cont = [1 0];

% Bounds
maxOutput = 10000000000;
maxInput = 0.2;

% Parameters
delta = 0.1;
N = 100;
n = size(A_cont,1);
m = size(B_cont,2);
q = size(C_cont,1);

% Discrete system
A = eye(n) + delta*A_cont;
B = delta*B_cont;
C = C_cont;

% Cost Function
Q = 10*eye(q);
R = eye(m);

% Initial state
x(:,1) = [pi; 0];

% Construct matrices
for k = 1:N
    for j = 1:k
        S_bar(q*(k-1)+(1:q),m*(k-j)+(1:m)) = C*A^(j-1)*B;
    end
    
    T_bar(q*(k-1)+(1:q),1:n) = C*A^k;
    
    Q_bar(q*(k-1)+(1:q),q*(k-1)+(1:q)) = Q;
    R_bar(m*(k-1)+(1:m),m*(k-1)+(1:m)) = R;
end

% Cost function matrices
H = 2*(R_bar + S_bar'*Q_bar*S_bar);
F_tra = 2*T_bar'*Q_bar*S_bar;

% Constraint matrices
G = [S_bar; -S_bar; eye(N*m); -eye(N*m)];
W = [kron(ones(N,1),maxOutput); kron(ones(N,1),maxOutput); kron(ones(N,1),maxInput); kron(ones(N,1),maxInput)];

% Run MPC
for i = 1:3000
    S = [-T_bar; T_bar; zeros(N,n); zeros(N,n)];
    
    u_star = quadprog(H, x(:,i)'*F_tra, G, W+S*x(:,i));
    u(:,i) = u_star(1:m);
%     x(:,i+1) = A*x(:,i) + B*u(:,i);
    x(:,i+1) = x(:,i) + delta*[x(2,i); sin(x(1,i))+u(:,i)];
    % All states wrapped to 2pi
    if x(1,i+1)>pi
        x(1,i+1) = -pi + (x(1,i+1)-pi);
    elseif x(1,i+1)<-pi
        x(1,i+1) = pi - (-pi - x(1,i+1));
    end
    
    clf
    subplot(1,2,1)
    hold on
    plot(x(1,:), 'lineWidth', 2)
    plot(x(2,:), 'lineWidth', 2)
    plot(u(1,:), 'lineWidth', 2)
    axis([1 3000 -4 4])
    legend('pos', 'vel', 'input')
    
    subplot(1,2,2)
    line([0  sin(x(1,i))], [0  cos(x(1,i))], 'lineWidth', 2)
    axis equal
    axis([-1 1 -1 1])
    
    drawnow
end

