clear variables 
close all 
clc

%% experiment
rng(100);
t_f        = 50; 
delta_t    = 0.01;  
t          = 0:delta_t:t_f;
init_state = [pi; pi/2; 0; 0];
%% controller

% reference

%q1des_t = pi/2*ones(1,length(t));
%q2des_t = pi/3*ones(1,length(t));

% sin traj
q1des_t = pi/2*sin(t);
q2des_t = pi/3*cos(t);

dq1des_t = diff(q1des_t)/delta_t;
dq1des_t = [dq1des_t , dq1des_t(end)];
dq2des_t = diff(q2des_t)/delta_t;
dq2des_t = [dq2des_t , dq2des_t(end)];

x_des    = [q1des_t;q2des_t;dq1des_t;dq2des_t];

% system 
A_cont       = [0 0 1 0 ; 0 0 0 1 ; 0 0 0 0; 0 0 0 0];
B_cont       = [0 0; 0 0; 1 0; 0 1];
C_cont       = eye(4);%[1 0 0 0; 0 1 0 0];
maxOutput    = [10;10;10;10];
maxInput     = [20;20];
N            = 30;      % prediction window
state_gain   = 100;     % penalty error on the state
control_cost = 1;
% regulator 
%controller = Ctrl.mpcRegulator(A_cont,B_cont,C_cont,maxInput,maxOutput,delta,N,state_gain,control_cost);
% tracker 
controller  = Ctrl.mpcTracker(A_cont,B_cont,C_cont,x_des,maxInput,maxOutput,delta_t,N,state_gain,control_cost);

%% env
reward = @(x,u)(norm(x));
env = Env.TwoRRobot(init_state,delta_t,reward,'ConfigFile','robot_payload');

%% estimated model
est_model = Env.EstimatedModel(env,'ConfigFile','robot_no_payload');

%% estimator
cov                  = 100;
noise                = 0.001;
measure_dim          = controller.m;
num_pars             = 8;
function_regressor   = "ModelEst.get_2R_Y_licols";
est                  = ModelEst.OLS(t,cov,noise,measure_dim,num_pars,function_regressor,1);
% converter from coefficients to param
update_payload_param = ModelEst.Robot2RPayloadParameters('robot_no_payload',est_model.orig_prm);
%% simulation loop
cur_x = init_state;
all_states(:,1) = cur_x;
for i=1:length(t)-1
    
    % model update
    if(i>1)
        est.Update(cur_x,last_action,last_DD,i);
        [param_list,value] = update_payload_param.ComputePayloadParameters(est.GetMean());
        est_model.UpdatePrm(param_list,value);
    end
    
    % mpc tracker
    u        = controller.ComputeControl(cur_x,i);
    % computed torque
    dyn_comp = est_model.ComputeDynamics(cur_x);
    tau      = dyn_comp.M*u + dyn_comp.S*cur_x(3:4,1) + dyn_comp.g;
    % model integration/home/claudio/Downloads/Invoice.pdf
    [new_state]= env.Step(tau);
    
    cur_x             = new_state;% + [0.4*randn(2,1) ; 0.4*randn(2,1)];
    all_states(:,i+1) = cur_x;
    last_action       = env.GetMeasuredAction() + 1*randn(2,1);
    last_DD           = env.GetMeasuredAcc();
end

%--------------------------------------------------------------------
% PLOTS
%--------------------------------------------------------------------
%%
figure
plot(t,all_states(1,:),t,all_states(2,:),t,q1des_t,t,q2des_t);
grid;
xlabel('time');
ylabel('pos [rad]');
legend('q_1','q_2','q_1 desired','q_2 desired');

mL_estimated = update_payload_param.all_payloads(1,:);
cLx_estimated = update_payload_param.all_payloads(2,:)./mL_estimated;
cLy_estimated = update_payload_param.all_payloads(3,:)./mL_estimated;
cLz_estimated = update_payload_param.all_payloads(4,:)./mL_estimated;
JLzz_estimated = update_payload_param.all_payloads(5,:);

ConfigFile.payload_2R;

figure
t_to_plot = t(1:end-2);
plot(t_to_plot,mL_estimated,t_to_plot,cLx_estimated,t_to_plot,cLy_estimated,t_to_plot,cLz_estimated,t_to_plot,JLzz_estimated);
leg_1 = sprintf('mL (real val = %f)',mL);
leg_2 = sprintf('cLx (real val = %f)',cLx);
leg_3 = sprintf('cLy (real val = %f)',cLy);
leg_4 = sprintf('cLz (real val = %f)',cLz);
leg_5 = sprintf('JLzz (real val = %f)',JLzz);
legend(leg_1,leg_2,leg_3,leg_4,leg_5);
grid;
xlabel('time');