function QLearnPolicyV1(rewardFunc,learnRate,epsilon,...
                       epsilonDecay,discount,successRate,...
                       winBonus,startPt,maxEpi,maxit,dt,tLim,...
                       state_bounds,quantization_step,envName)
    %% Example reinforcement learning - Q-learning code
    % Learn a control policy to optimally swing a pendulum from vertical down,
    % to vertical up with torque limits and (potentially) noise. Both the
    % pendulum and the policy are animated as the process is going. The
    % difference from dynamic programming, for instance, is that the policy is
    % learned only by doing forward simulation. No knowledge of the dynamics is
    % used to make the policy.
    %   
    % Play around with the learning settings below. I'm sure they could be
    % improved greatly!
    %
    %   Video: https://www.youtube.com/watch?v=YLAWnYAsai8
    %
    %   Matthew Sheen, 2015
    %
    %% set up environment
    commmand = "Env." + envName + "(x(:,1)',dt,rewardFunc);";
    env = eval(commmand);
    %env = Env.InvPend(startPt,dt,rewardFunc);
    env.Render();
    
    %update bound
    
    %% set up action set
    actions = [0, -tLim, tLim]; % Only 3 options, Full blast one way, the other way, and off.

    %% Discretize the state so we can start to learn a value map
    % State1 is angle -- play with these for better results. Faster convergence
    % with rough discretization, less jittery with fine.
    x = cell{1,env.n_state};
    for i = 1:env.n_state
        x{1,i} = env.state_bounds(i,1):quantization_step(i):env.state_bounds(i,2);
        %State2 angular rate
        %x2 = -pi:0.1:pi;
    end

    %Generate a state list
    

    % Local value R and global value Q -- A very important distinction!
    %
    % R, the reward, is like a cost function. It is good to be near our goal. It doesn't
    % account for actions we can/can't take. We use quadratic difference from the top.
    %
    % Q is the value of a state + action. We have to learn this at a global
    % level. Once Q converges, our policy is to always select the action with the
    % best value at every state.
    %
    % Note that R and Q can be very, very different.
    % For example, if the pendulum is near the top, but the motor isn't
    % powerful enough to get it to the goal, then we have to "pump" the pendulum to
    % get enough energy to swing up. In this case, that point near the top
    % might have a very good reward value, but a very bad set of Q values since
    % many, many more actions are required to get to the goal.
    %dummyu = 0;
    R = rewardFunc(states,dummyu); % Initialize the "cost" of a given state to be quadratic error from the goal state. Note the signs mean that -angle with +velocity is better than -angle with -velocity
    Q = repmat(R,[1,3]); % Q is length(x1) x length(x2) x length(actions) - IE a bin for every action-state combination.


    % V will be the best of the Q actions at every state. This is only
    % important for my plotting. Not algorithmically significant. I leave
    % values we haven't updated at 0, so they appear as blue (unexplored) areas
    % in the plot.
    V = zeros(size(states,1),1);
    Vorig = reshape(max(Q,[],2),[length(x2),length(x1)]);
    
    %% visualization and video setting
    % Make the un-updated values on the value map transparent. If not, then
    % we see the reward function underneath.
    transpMap = true;

    % Write to video?
    doVid = false;

    if doVid
        writerObj = VideoWriter('qlearnVid.mp4','MPEG-4');
        writerObj.FrameRate = 60;
        open(writerObj);
    end
    
    if(env.num_state<=2)
        % Set up the state-value map plot (displays the value of the best action at every point)
        panel = figure;
        colormap('hot');
        subplot(1,4,[2:4]);
        hold on
        map = imagesc(reshape(R,[length(x2),length(x1)]));
        axMap = map.Parent;
        axMap.XTickLabels = {'-pi' '0' 'pi'};
        axMap.XTick = [1 floor(length(x1)/2) length(x1)];
        axMap.YTickLabels = {'-pi' '0' 'pi'};
        axMap.YTick = [1 floor(length(x2)/2) length(x2)];
        axMap.XLabel.String = 'Angle (rad)';
        axMap.YLabel.String = 'Angular rate (rad/s)';
        axMap.Visible = 'on';
        axMap.Color = [0.3 0.3 0.5];
        axMap.XLim = [1 length(x1)];
        axMap.YLim = [1 length(x2)];
        axMap.Box = 'off';
        axMap.FontSize = 14;
        caxis([3*min(R),max(R)])
        pathmap = plot(NaN,NaN,'.g','MarkerSize',30); % The green marker that travels through the state map to match the pendulum animation
        map.CData = V;
        hold off
    end

    %% Start learning!

    % Number of episodes or "resets"
    for episodes = 1:maxEpi

        z1 = startPt; % Reset the pendulum on new episode.
        env.Reset()
        % Number of actions we're willing to try before a reset
        for g = 1:maxit

            % Stop if the figure window is closed.
            if ~ishandle(panel)
                break;
            end

            %% PICK AN ACTION

            % Interpolate the state within our discretization (ONLY for choosing
            % the action. We do not actually change the state by doing this!)
            [~,sIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2));

            % Choose an action:
            % EITHER 1) pick the best action according the Q matrix (EXPLOITATION). OR
            % 2) Pick a random action (EXPLORATION)
            if (rand()>epsilon || episodes == maxEpi) && rand()<=successRate % Pick according to the Q-matrix it's the last episode or we succeed with the rand()>epsilon check. Fail the check if our action doesn't succeed (i.e. simulating noise)
                [~,aIdx] = max(Q(sIdx,:)); % Pick the action the Q matrix thinks is best!
            else
                aIdx = randi(length(actions),1); % Random action!
            end

            T = actions(aIdx);

            %% STEP DYNAMICS FORWARD

            % Step the dynamics forward with our new action choice
            % RK4 Loop - Numerical integration
            [new_state, reward, done] = env.Step(T);
            z1 = new_state; % Old state = new state


            %% UPDATE Q-MATRIX

            % End condition for an episode
            if norm(new_state)<0.01 % If we've reached upright with no velocity (within some margin), end this episode.
                success = true;
                bonus = winBonus; % Give a bonus for getting there.
            else
                bonus = 0;
                success = false;
            end

            [~,snewIdx] = min(sum((states - repmat(z1,[size(states,1),1])).^2,2)); % Interpolate again to find the new state the system is closest to.

            if episodes ~= maxEpi % On the last iteration, stop learning and just execute. Otherwise...
                % Update Q
                Q(sIdx,aIdx) = Q(sIdx,aIdx) + learnRate * ( reward + discount*max(Q(snewIdx,:)) - Q(sIdx,aIdx) + bonus );

                % Lets break this down:
                %
                % We want to update our estimate of the global value of being
                % at our previous state s and taking action a. We have just
                % tried this action, so we have some information. Here are the terms:
                %   1) Q(sIdx,aIdx) AND later -Q(sIdx,aIdx) -- Means we're
                %      doing a weighting of old and new (see 2). Rewritten:
                %      (1-alpha)*Qold + alpha*newStuff
                %   2) learnRate * ( ... ) -- Scaling factor for our update.
                %      High learnRate means that new information has great weight.
                %      Low learnRate means that old information is more important.
                %   3) R(snewIdx) -- the reward for getting to this new state
                %   4) discount * max(Q(snewIdx,:)) -- The estimated value of
                %      the best action at the new state. The discount means future
                %      value is worth less than present value
                %   5) Bonus - I choose to give a big boost of it's reached the
                %      goal state. Optional and not really conventional.
            end

            % Decay the odds of picking a random action vs picking the
            % estimated "best" action. I.e. we're becoming more confident in
            % our learned Q.
            epsilon = epsilon*epsilonDecay;

            %% UPDATE PLOTS

            if episodes>0 && (env.num_state<=2)


                % Green tracer point:
                [newy,newx] = ind2sub([length(x2),length(x1)],snewIdx); % Find the 2d index of the 1d state index we found above
                set(pathmap,'XData',newx);
                set(pathmap,'YData',newy);

                % The heat map of best Q values
                V = max(Q,[],2); % Best estimated value for all actions at each state.
                fullV = reshape(V,[length(x2),length(x1)]); % Make into 2D for plotting instead of a vector.
                set(map,'CData',fullV);
                if transpMap
                    set(map,'AlphaData',fullV~=Vorig); % Some spots have not changed from original. If not, leave them transparent.
                end
                drawnow;

                % Take a video frame if turned on.
                if doVid
                    frame = getframe(panel);
                    writeVideo(writerObj,frame);
                end
            end

            % End this episode if we've hit the goal point (upright pendulum).
            if success
                break;
            end

        end
    end

    if doVid
        close(writerObj);
    end

end
