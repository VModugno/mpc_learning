function B_sym = get_B(T,dq)
    disp('...Computing B(q)');
    B_sym = collect(simplify(hessian(T,dq)));
end