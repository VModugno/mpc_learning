function T = get_Kinetic_Energy_Classical_DH_revised(dq,A,sigma,r_i_ci,masses,J_tensor)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTING KINETIC ENERGY (Moving Frames Alg.)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V_i = sym('V_i', [3 7]);
W_i = sym('W_i', [3 7]);

n = length(dq);

T = 0;
for i=1:n
    
    string_to_disp = sprintf('...Kinetic Energy Algorithm: joint %d\n',i);
    disp(string_to_disp);
    
    if i==1
        v_im1_im1 = [0;0;0];
        w_im1_im1 = [0;0;0];
    else
        v_im1_im1 = V_i(:,i-1);
        w_im1_im1 = W_i(:,i-1);
    end
    
    R_im1_i = A{i}(1:3,1:3);
    r_im1_im1_i = A{i}(1:3,4);
    
    w_im1_i = w_im1_im1 + (1-sigma(i))*dq(i)*[0;0;1];
    w_i_i = simplify (R_im1_i' * w_im1_i);
    v_i_i = simplify (R_im1_i' * (v_im1_im1 + sigma(i)*dq(i)*[0;0;1] + cross(w_im1_i,r_im1_im1_i)));
    %v_ci =  simplify (v_i_i + cross(w_i_i,r_i_ci(:,i)));
    
    %T = T + collect(simplify( 0.5 * v_ci' * masses(i) * v_ci + 0.5 * w_i_i' * I_tensor{i} * w_i_i )); 

    m_i = masses(i);
    MS_i = m_i*r_i_ci(:,i);
    
    T = T + collect(simplify( 0.5 * v_i_i' * masses(i) * v_i_i ...
        + 0.5 * w_i_i' * J_tensor{i} * w_i_i ...
        + MS_i'*cross(v_i_i,w_i_i))); 

    V_i(:,i) = v_i_i;
    W_i(:,i) = w_i_i;
end
T = collect(simplify(T));
end