% 2018-06-06 file created
% lo scopo di questo file è costruire il modello dinamico simbolico del
% robot Staubli RX 160 utilizzando la convenzione di
% Denavit-Hartenberg CLASSICA RIVISITATA (con le inerzie rispetto ai frame)

clear all
clc
close all

tic
[B_sym,S_sym,g_sym,tau_sym] = get_Staubli_Symb_B_S_g();
toc

description = ['6 giugno 2018'];

date = clock;
workspacefilename = sprintf('%d-%d-%d-%d-%d',date(1:5));
save(workspacefilename);

%%
h = mupad;
setVar(h,'M',B_sym);
setVar(h,'g',g_sym);
setVar(h,'S',S_sym);
setVar(h,'tau',tau_sym);