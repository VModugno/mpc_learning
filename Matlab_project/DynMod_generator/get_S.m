function S_sym = get_S(q,dq,B_sym)
    disp('...Computing S(q)');
    n = length(q);
    S_sym = sym('S_sym',n);
    for k=1:n
        dBkdq = simplify(jacobian(B_sym(:,k),q));
        dBdqk = simplify(diff(B_sym,q(k)));
        S_sym(k,:) = dq' * 1/2 * (dBkdq + dBkdq' - dBdqk);
    end
    S_sym = collect(simplify(S_sym));
end