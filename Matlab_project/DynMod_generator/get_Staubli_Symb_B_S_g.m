% 2018-06-06 file created

%% get_Staubli_Symb_B_g computes symbolically matrix B(q) and g(q)

function [B_sym,S_sym,g_sym,tau_sym] = get_Staubli_Symb_B_S_g()
 
syms q1 q2 q3 q4 q5 q6 dq1 dq2 dq3 dq4 dq5 dq6 ddq1 ddq2 ddq3 ddq4 ddq5 ddq6 real
syms c1x c1y c1z c2x c2y c2z c3x c3y c3z c4x c4y c4z c5x c5y c5z c6x c6y c6z real

syms J1xx J1xy J1xz J1yy J1yz J1zz real
syms J2xx J2xy J2xz J2yy J2yz J2zz real
syms J3xx J3xy J3xz J3yy J3yz J3zz real
syms J4xx J4xy J4xz J4yy J4yz J4zz real
syms J5xx J5xy J5xz J5yy J5yz J5zz real
syms J6xx J6xy J6xz J6yy J6yz J6zz real

syms a1 a2 d1 d4 d6 real
syms m1 m2 m3 m4 m5 m6 real
syms g0 real
syms t real

%% Assumptions
% no assumption made

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%               DATA STRUCTURES
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

q = [q1;q2;q3;q4;q5;q6];
dq = [dq1;dq2;dq3;dq4;dq5;dq6];
ddq = [ddq1;ddq2;ddq3;ddq4;ddq5;ddq6];

g_vect = [0;0;-g0];

r_1_c1 = [c1x;c1y;c1z];
r_2_c2 = [c2x;c2y;c2z];
r_3_c3 = [c3x;c3y;c3z];
r_4_c4 = [c4x;c4y;c4z];
r_5_c5 = [c5x;c5y;c5z];
r_6_c6 = [c6x;c6y;c6z];
r_i_ci = [r_1_c1 , r_2_c2 , r_3_c3 , r_4_c4 , r_5_c5 , r_6_c6];

masses = [m1 m2 m3 m4 m5 m6];

J1 = [J1xx,J1xy,J1xz ; J1xy,J1yy,J1yz ; J1xz,J1yz,J1zz];
J2 = [J2xx,J2xy,J2xz ; J2xy,J2yy,J2yz ; J2xz,J2yz,J2zz];
J3 = [J3xx,J3xy,J3xz ; J3xy,J3yy,J3yz ; J3xz,J3yz,J3zz];
J4 = [J4xx,J4xy,J4xz ; J4xy,J4yy,J4yz ; J4xz,J4yz,J4zz];
J5 = [J5xx,J5xy,J5xz ; J5xy,J5yy,J5yz ; J5xz,J5yz,J5zz];
J6 = [J6xx,J6xy,J6xz ; J6xy,J6yy,J6yz ; J6xz,J6yz,J6zz];

I_tensor = {J1,J2,J3,J4,J5,J6};

sigma = zeros(1,6); % all joints are revolute

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTING TRANSFORMATION MATRICES (KINEMATICS)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

string_to_disp = sprintf('STARTING ALGORITHM...\n');
disp(string_to_disp);

% a , alpha[rad] , d , theta[rad]
A1=DH(a1,pi/2,d1,q1);
A2=DH(a2,0,0,q2);
A3=DH(0,pi/2,0,q3);
A4=DH(0,pi/2,d4,q4);
A5=DH(0,-pi/2,0,q5);
A6=DH(0,0,d6,q6);

A = {A1,A2,A3,A4,A5,A6};

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTING KINETIC AND POTENTIAL ENERGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

T = get_Kinetic_Energy_Classical_DH_revised(dq,A,sigma,r_i_ci,masses,I_tensor);
%T = get_Kinetic_Energy(dq,A,sigma,r_i_ci,masses,I_tensor);
U = get_Potential_Energy(A,r_i_ci,masses,g_vect);

B_sym = get_B(T,dq);

g_sym = get_g(U,q);

S_sym = get_S(q,dq,B_sym);

tau_sym = B_sym*ddq + S_sym*dq + g_sym;
%save ('KUKA_iiwa_DynMod_sym_S','S_sym');
end