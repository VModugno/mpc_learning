function g_sym = get_g(U,q)
    disp('...Computing g(q)');
    g_sym = collect(simplify(jacobian(U,q)'));
end