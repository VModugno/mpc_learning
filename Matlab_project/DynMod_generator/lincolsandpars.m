% 2016-10-13
% @author Claudio Gaz
% Prune linear dependent columns of a matrix Y
% This function returns a matrix with the independent columns of Y, the modified ''dynamic parameters'' vector,
% which is, after this procedure, the ''dynamic coefficients'' vector, and the index of the independent columns of Y
function [Yli,Pli,li_cols_idx] = lincolsandpars(Y,P,tol)

    if nargin<3
        tol=1e-10;
    end
    
    [rref_mat,li_cols_idx] = rref(Y,tol);
    Yli = Y(:, li_cols_idx);

    newP = rref_mat*P;
    Pli = non_zero_elements(newP);
end