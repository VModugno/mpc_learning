function U = get_Potential_Energy(A,r_i_ci,masses,g_vect)
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% COMPUTING POTENTIAL ENERGY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
n = length(masses);
U = 0;
for i=1:n
    
    string_to_disp = sprintf('...Potential Energy Algorithm: joint %d\n',i);
    disp(string_to_disp);
    
    A_curr = eye(4,4);
    for j=1:i
        A_curr = A_curr*A{j};
    end
    r_curr = [r_i_ci(:,i);1];
    r0_ci_hom = A_curr*r_curr;
    r0_ci = collect(simplify(r0_ci_hom(1:3,:)));
    m_i = masses(i);
    U_curr = simplify(-m_i*g_vect'*r0_ci);
    
    U = U + U_curr;
end

U = collect(simplify(U));
end