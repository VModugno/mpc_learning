% Online Least Square
classdef OLS < handle
    
    properties 
        PP 
        KK 
        XX 
        RR
        num_pars
        measure_dim
        regressor
        cur_it
        cov_res
    end
    
    methods
        function  obj = OLS(t,cov,noise,measure_dim,num_pars,function_regressor,cov_res)
            obj.PP        = cell(length(t),1);
            obj.KK        = cell(length(t),1);
            obj.XX        = zeros(num_pars,length(t));
            obj.PP{1}     = cov*eye(num_pars);
            obj.RR        = noise*eye(measure_dim,measure_dim); 
            obj.num_pars  = num_pars;
            obj.cur_it    = 1;
            obj.regressor = str2func(function_regressor);
            obj.cov_res   = cov_res;
        end
        
        function Update(obj,mes_state,mes_action,mes_direct_dynamic,it)
            % in this function action comes from the previous iteration
            H_ols        = obj.regressor(mes_state,mes_direct_dynamic);
            obj.KK{it}   = (obj.PP{it-1}*H_ols')/(H_ols*obj.PP{it-1}*H_ols'+obj.RR);
            obj.PP{it}   = (eye(obj.num_pars)-obj.KK{it}*H_ols)*obj.PP{it-1};
            obj.XX(:,it) = obj.XX(:,it-1)+obj.KK{it}*(mes_action-H_ols*obj.XX(:,it-1));
            obj.cur_it   = it;
            
            if obj.cov_res && trace(obj.PP{it}) < 1e-03
                obj.PP{it} = obj.PP{1};
                disp(it);
                pause(1);
            end
        end
        
        function XX = GetMean(obj)
            XX = obj.XX(:,obj.cur_it);
        end
        
        function PP = GetCov(obj)
            PP = obj.PP{obj.cur_it};
        end 
    end
    
    
end