from numpy import *
import os
#from pylab import *
import numpy as np
#import matplotlib.pyplot as plt
#import matplotlib.cbook as cbook
import time
#from scipy.misc import imread
#from scipy.misc import imresize
#import matplotlib.image as mpimg
#from scipy.ndimage import filters
import urllib
#from numpy import random
import tensorflow as tf
#import tflearn

class Actor_Net():
  
  def __init__(self,size_state,size_act,bound_act,batch_size,session):    
         
        
        self.input = tf.placeholder(shape=[None, size_state], dtype=tf.float32, name="X")
        #self.action_from_demonstration = tf.placeholder(shape=[None,size_act], dtype=tf.float32, name="actions_from_demonstration")


    	#rete principale actor-----------#
        self.W_fc1 = tf.Variable(tf.truncated_normal([size_state, 400],stddev=0.01))
        self.b_fc1 = tf.Variable(tf.zeros(400))

        self.W_fc2 = tf.Variable(tf.truncated_normal([400, 300],stddev=0.01))
        self.b_fc2 = tf.Variable(tf.zeros(300))

        self.W_fc3 = tf.Variable(tf.truncated_normal([300, size_act],stddev=0.003))
        self.b_fc3 = tf.Variable(tf.zeros(size_act))


        self.network_params = tf.trainable_variables()
        
        epsilon = 0.001

        z_1 =  tf.matmul(self.input, self.W_fc1) + self.b_fc1
        #batch_mean1, batch_var1 = tf.nn.moments(z_1,[1])
        #scale1 = tf.Variable(tf.ones([size_state, 400]))
        #beta1 = tf.Variable(tf.zeros([400]))
        #LN1 = tf.nn.batch_normalization(z_1,batch_mean1,batch_var1,None,None,epsilon)
        #LN1 = tf.contrib.layers.batch_norm(z_1)
        self.out_fc1 = tf.nn.relu(z_1)
        
        z_2 =  tf.matmul(self.out_fc1, self.W_fc2) + self.b_fc2
        #batch_mean2, batch_var2 = tf.nn.moments(z_2,[1])
        #scale2 = tf.Variable(tf.ones([400, 300]))
        #beta2 = tf.Variable(tf.zeros([300]))
        #LN2 = tf.nn.batch_normalization(z_2,batch_mean2,batch_var2,None,None,epsilon)
        #LN2 = tf.layers.batch_normalization(z_2)
        self.out_fc2 = tf.nn.relu(z_2)

        z_3 =  tf.matmul(self.out_fc2, self.W_fc3) + self.b_fc3
        #batch_mean3, batch_var3 = tf.nn.moments(z_3,[1])
        #scale3 = tf.Variable(tf.ones([300, size_act]))
        #beta3 = tf.Variable(tf.zeros([size_act]))
        #LN3 = tf.nn.batch_normalization(z_3,batch_mean3,batch_var3,None,None,epsilon)    
        self.out_fc3 = tf.nn.tanh(z_3)
 
        self.scaled_out = tf.multiply(self.out_fc3, bound_act)

        ###FROM IMITATION LEARNING
        self.saver = tf.train.Saver([self.W_fc1, self.b_fc1, self.W_fc2, self.b_fc2,self.W_fc3, self.b_fc3])
        self.to_inizialize_Actor = tf.variables_initializer([self.W_fc1, self.b_fc1, self.W_fc2, self.b_fc2,self.W_fc3, self.b_fc3])
        session.run(self.to_inizialize_Actor)
        checkpoint = tf.train.get_checkpoint_state("saved_networks_ddpg_5")
        if checkpoint and checkpoint.model_checkpoint_path:
            self.saver.restore(session, checkpoint.model_checkpoint_path)
            print("Successfully loaded:", checkpoint.model_checkpoint_path)
        else:
            print("Could not find old network weights")
        ##########################
        
        #target--------------------------#
        '''self.W_fc1_target = tf.Variable(tf.truncated_normal([size_state, 400],stddev=0.01))
        self.b_fc1_target = tf.Variable(tf.zeros(400))

        self.W_fc2_target = tf.Variable(tf.truncated_normal([400, 300],stddev=0.01))
        self.b_fc2_target = tf.Variable(tf.zeros(300))

        self.W_fc3_target = tf.Variable(tf.truncated_normal([300, size_act],stddev=0.003))
        self.b_fc3_target = tf.Variable(tf.zeros(size_act))'''
        self.W_fc1_target = tf.Variable(self.W_fc1.initialized_value())
        self.b_fc1_target = tf.Variable(self.b_fc1.initialized_value())

        self.W_fc2_target = tf.Variable(self.W_fc2.initialized_value())
        self.b_fc2_target = tf.Variable(self.b_fc2.initialized_value())

        self.W_fc3_target = tf.Variable(self.W_fc3.initialized_value())
        self.b_fc3_target = tf.Variable(self.b_fc3.initialized_value())

        
        self.target_network_params = tf.trainable_variables()[
            len(self.network_params):]

        self.out_fc1_target = tf.nn.relu(tf.matmul(self.input, self.W_fc1_target) + self.b_fc1_target)
        

        self.out_fc2_target = tf.nn.relu(tf.matmul(self.out_fc1_target, self.W_fc2_target) + self.b_fc2_target)
        self.out_fc3_target = tf.nn.tanh(tf.matmul(self.out_fc2_target, self.W_fc3_target) + self.b_fc3_target)
 
        self.scaled_out_target = tf.multiply(self.out_fc3_target, bound_act)
        

        #------------------------------#
        #funzione per updare i pesi della rete target e la rete principale - presa online
        self.update_target_network_params = \
            [self.target_network_params[i].assign(tf.multiply(self.network_params[i], 0.001) +
                                                  tf.multiply(self.target_network_params[i], 1. - 0.001))
                for i in range(len(self.target_network_params))]
        #-----------------------------#

        
        #ricevo il gradiente dal critic
        self.action_gradient = tf.placeholder(tf.float32, [None, size_act])
        
        self.actor_qvalue = tf.placeholder(tf.float32, [None, 1])
        self.critic_loss = -tf.reduce_mean(self.actor_qvalue)
        
        #self.actor_loss = -tf.reduce_mean(self.action_gradient)
        #Combino i gradienti
        #self.actor_gradients = tf.gradients(tf.reduce_mean(self.out_fc3,axis = 0),self.network_params,-self.action_gradient)
        self.actor_gradients_unnormalized = tf.gradients(self.out_fc3,self.network_params,-self.action_gradient)
        self.actor_gradients = list(map(lambda x: tf.div(x, batch_size), self.actor_gradients_unnormalized))
        #self.actor_gradients = list(map(lambda x: tf.div(x, bound_act), self.actor_gradients_))
        #self.actor_gradients = tf.gradients(self.scaled_out,self.network_params,-self.action_gradient)
        #self.actor_gradients = tf.gradients(self.out_fc3,self.network_params,-self.action_gradient)
        
        #regularizer = tf.nn.l2_loss(self.W_fc1)
        #regularizer_2 = tf.nn.l2_loss(self.W_fc2)
        #regularizer_3 = tf.nn.l2_loss(self.W_fc3)

        #per la backprop
        #self.loss = tf.reduce_mean(tf.square(self.scaled_out - self.action_from_demonstration))
        #self.demonstration_gradient = tf.gradients(self.loss, self.network_params)
        with tf.variable_scope('adam_Actor') as net_build:
            self.optimize_ = tf.train.AdamOptimizer(0.0001)
            self.optimize = self.optimize_.\
                apply_gradients(zip(self.actor_gradients, self.network_params))
        #self.optimize = tf.train.AdamOptimizer(0.0001).minimize(self.loss)
        self.num_trainable_vars = len(self.network_params) + len(self.target_network_params)
        

  def get_num_trainable_vars(self):
      return self.num_trainable_vars

      
