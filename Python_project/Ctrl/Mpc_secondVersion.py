import numpy as np
from numpy.linalg import inv
import math
from scipy.linalg import block_diag
from numpy import linalg as LA
from qpoases import PyQProblem as QProblem
from qpoases import PyOptions as Options
from qpoases import PyPrintLevel as PrintLevel
from qpoases import PyBooleanType as BooleanType
from qpoases import *

class Mpc_RL(object):
    def __init__(self,):
        #double m1;   //link 1 mass
        #double m2;   //link 2 mass
        #double a1;   //link 1 length (distance from joint 1 axis and joint 2 axis
        #double a2;   //link 2 length
        #double l1;   //distance between link 1 center of mass and joint 1
        #double l2;   //distance between link 2 center of mass and joint 2
        #double Im;   //inertia moment of the motor arount its axis
        #double Il1;  //inertia moment of link 1 arount joint 1 axis
        #double Il2;  //inertia moment of link 2 arount joint 2 axis
        m1 = 0.193  #Kg   
        m2 = 0.073  #Kg
        a1 = 0.1483 #m 
        a2 = 0.1804 #m  
        l1 = 0.1032 #m
        l2 = 0.1065 #m
        Im = 4.74e-4
        Il1 = m1*a1*a1/12
        Il2 = m2*a2*a2/12
        #definition of the dynamic parameters
        self.A1 = Il1 + Il2 + Im + m1*l1*l1 + m2*(a1*a1 + l2*l2)
        self.A2 = Il2 + m2*l2*l2
        self.A3 = m2*a1*l2
        self.A4 = m1*l1 + m2*a1
        self.A5 = m2*l2

        #MPC utils
        self.N_step = 50
        self.delta_mpc = 0.00001
        self.state_dim = 5

        self.state_ref = 4

        self.A_list = []
        self.B_list = []
        self.init_states_list = []
        self.ref_value = []
        self.init_states_list_full = []
    
    def storeAmatrix(self,state,inputs):
        q1 = state[0]
        q2 = state[1]
        #print("stat",state)
        #print("q1",q1)
        #print("q2",q2)
        tau = inputs[0]
        q1dot = state[2]
        q2dot = state[3]
        A = np.zeros((4, 4))
        ####TO CHANGE
        A[0][2] = 1
        A[1][3] = 1
        ###########
        #-(490*a3*a5*cos(q1 + 2*q2) - 981*a2*a4*cos(q1) + 490*a3*a5*cos(q1))/(100*(a3^2*cos(q2)^2 - a1*a2 + a2^2))
        A[2][0] = -(490*self.A3*self.A5*math.cos(q1 + 2*q2) - 981*self.A2*self.A4*math.cos(q1) + 490*self.A3*self.A5*math.cos(q1))/(100*(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)))
        #(a3*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - ((a3*cos(q2)*q1dot^2 + (49*a5*cos(q1 + q2))/5)*(a2 + a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (a2*(a3*cos(q2)*q2dot^2 + 2*a3*q1dot*cos(q2)*q2dot - (49*a5*cos(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (2*a2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2 - (2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5)*(a2 + a3*cos(q2)))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2
        A[2][1] = (self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - ((self.A3*math.cos(q2)*math.pow(q1dot,2) + (49*self.A5*math.cos(q1 + q2))/5)*(self.A2 + self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (self.A2*(self.A3*math.cos(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.cos(q2)*q2dot - (49*self.A5*math.cos(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (2*self.A2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2) - (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5)*(self.A2 + self.A3*math.cos(q2)))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2)
        #-(2*a3*sin(q2)*(a2*q1dot + a2*q2dot + a3*q1dot*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[2][2] = -(2*self.A3*math.sin(q2)*(self.A2*q1dot + self.A2*q2dot + self.A3*q1dot*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        #(2*a2*a3*sin(q2)*(q1dot + q2dot))/(a1*a2 + a3^2*sin(q2)^2 - a2^2 - a3^2)
        A[2][3] = (2*self.A2*self.A3*math.sin(q2)*(q1dot + q2dot))/(self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.sin(q2),2) - math.pow(self.A2,2) - math.pow(self.A3,2))
        #(49*a5*cos(q1 + q2)*(a1 + 2*a3*cos(q2)))/(5*(a3^2*cos(q2)^2 - a1*a2 + a2^2)) - ((a2 + a3*cos(q2))*((49*a5*cos(q1 + q2))/5 + (981*a4*cos(q1))/100))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][0] =(49*self.A5*math.cos(q1 + q2)*(self.A1 + 2*self.A3*math.cos(q2)))/(5*(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))) - ((self.A2 + self.A3*math.cos(q2))*((49*self.A5*math.cos(q1 + q2))/5 + (981*self.A4*math.cos(q1))/100))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        # ((a2 + a3*cos(q2))*(a3*cos(q2)*q2dot^2 + 2*a3*q1dot*cos(q2)*q2dot - (49*a5*cos(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) + ((a3*cos(q2)*q1dot^2 + (49*a5*cos(q1 + q2))/5)*(a1 + 2*a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (a3*sin(q2)*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (2*a3*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) + (2*a3^2*cos(q2)*sin(q2)*(a2 + a3*cos(q2))*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2 + (2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5)*(a1 + 2*a3*cos(q2)))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2
        A[3][1] = ((self.A2 + self.A3*math.cos(q2))*(self.A3*math.cos(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.cos(q2)*q2dot - (49*self.A5*math.cos(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) + ((self.A3*math.cos(q2)*math.pow(q1dot,2) + (49*self.A5*math.cos(q1 + q2))/5)*(self.A1 + 2*self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (2*self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) + (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A2 + self.A3*math.cos(q2))*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2) + (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5)*(self.A1 + 2*self.A3*math.cos(q2)))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2)
        #(2*a3*sin(q2)*(a1*q1dot + a2*q2dot + 2*a3*q1dot*cos(q2) + a3*q2dot*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][2] = (2*self.A3*math.sin(q2)*(self.A1*q1dot + self.A2*q2dot + 2*self.A3*q1dot*math.cos(q2) + self.A3*q2dot*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        #(2*a3*sin(q2)*(q1dot + q2dot)*(a2 + a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][3] =(2*self.A3*math.sin(q2)*(q1dot + q2dot)*(self.A2 + self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        #A = (np.eye(self.state_dim) + A)*self.delta_mpc
        #self.A_list.append(A)
        B = np.zeros((4, 1))
        B[2][0] = -self.A2/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        B[3][0] = (self.A2 + self.A3*math.cos(q2))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        A_extended = np.zeros((5, 5))
        A_extended[0:4,0:4] = A
        #print("A_",A_extended)
        A_extended[0:4,4:5] = B
        #print("B___",B)
        A_extended[4,4] = 1
        #print("A________",A_extended)
        A_extended = np.eye(self.state_dim) + (A_extended)*self.delta_mpc
        print("A_Matrix",A_extended)
        self.A_list.append(A_extended)


    def storeBmatrix(self,state,inputs):
        q1 = state[0]
        q2 = state[1]
        tau = inputs[0]
        q1dot = state[2]
        q2dot = state[3]
        B = np.zeros((4, 1))
        B[2][0] = -self.A2/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        B[3][0] = (self.A2 + self.A3*math.cos(q2))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        B_extended = np.ones((5, 1))
        B_extended[0:4,:] = B#*self.delta_mpc
        #print("operazioni")
        #print("dividendo",(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)))
        #print("B_Matrix before",B_extended)
        B_extended = B_extended*self.delta_mpc
        print("B_Matrix",B_extended)
        self.B_list.append(B_extended)

    def storeState_Refvalue(self,state,reference):
        #print("Stateappe",state)
        #print("Stateappe",state[0])
        #print("Stateappe",state[1])
        #print("ref",reference)
        new_s = np.asarray([state[0],state[1]])
        #print("new_s",new_s)

        self.init_states_list.append(new_s)
        self.init_states_list_full.append(state)
        self.ref_value.append(reference)
    
    def flush(self,):
        self.A_list = []
        self.B_list = []
        self.init_states_list = []
        self.ref_value = []
        self.init_states_list_full = []
    
    def compute_tau(self):
        #print("Compute Tau")
        '''q1 = state[0]
        q2 = state[1]
        tau = inputs[0]
        q1dot = state[2]
        q2dot = state[3]'''
        #########moltiplicare A,B,C per T
        C = np.zeros((2, 5))
        C[0][0] = 1
        C[1][1] = 1    
        #########separare calcolo A,B,C con questo
        S_bar = np.zeros((2*self.N_step,1*self.N_step))
        T_bar = np.zeros((2*self.N_step,self.state_dim))
        P = block_diag(1,1)
        Q = block_diag(100, 100)
        R = 1000
        Q_bar = Q
        R_bar = R        
        for i in range(0,self.N_step):
            for j in range(0,self.N_step):
                #print("riga",i)
                #print("colonna",j)
                #print("differenza",(i-j))
                #A = self.A_list[i-j]
                #B = self.B_list[i-j]
                if((j-i) < 0):
                    #print("entro!")
                    ###COGLIONE
                    A = self.A_list[(j+1)]
                    #print("moltiplica A_index",j+1)
                    for z in range(j+2,(i+1)):
                        #print("per A_index",z)
                        A_temp = self.A_list[z]
                        A = np.dot(A_temp,A)             
                    B = self.B_list[j]
                    #print("Inserisco da riga",0 + 2*i)
                    #print("a riga",2 + 2*i)
                    #print("####")
                    #print("Inserisco da colonna",0 + 1*j)
                    #print("a colonna",1 + 1*j)
                    S_bar[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = np.dot(C,np.dot(A,B))
                    ####
                elif((j-i) == 0):
                    #print("entro solo per B!")
                    B = self.B_list[j]
                    S_bar[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = np.dot(C,B)
                else:
                    S_bar[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = 0.0
                #print("##########")
            #####COGLIONE2
            A = self.A_list[0]
            for z in range(1,(i+1)):
                A_temp = self.A_list[z]
                A = np.dot(A_temp,A)  
            T_bar[0 + 2*i:2 + 2*i,:] = np.dot(C,A)
            #######################
            if(i == (self.N_step -1)):
                break
            R_bar = block_diag(R_bar, R)
            if(i < (self.N_step - 2)):
                Q_bar = block_diag(Q_bar, Q)
        #stato finale - forse una volta di troppo
        Q_bar = block_diag(Q_bar,P)
        #print("T_bar",T_bar)
        #print("S_bar",S_bar)
        #print("Q_bar",Q_bar)
        #print("R_bar",R_bar)

        ###Calcolo per delta_u - delta_u_ref
        S_bar_u = np.zeros((1*self.N_step,1*self.N_step))
        T_bar_u = np.zeros((1*self.N_step,1))
        W_u = 10000
        W_delta_u = 1000
        W_u_bar = W_u
        W_delta_u_bar = W_delta_u
        for i in range(0,self.N_step):
            for j in range(0,self.N_step):
                if((j-i) <= 0):
                    S_bar_u[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = 1
            T_bar_u[0 + 1*i:1 + 1*i,:] = 1
            if(i == (self.N_step -1)):
                break
            W_u_bar = block_diag(W_u_bar, W_u)
            W_delta_u_bar = block_diag(W_delta_u_bar, W_delta_u)
        #print("S_bar_u",S_bar_u)
        #print("T_bar_u",T_bar_u)
        #print("W_delta_u_bar",W_delta_u_bar)
        #H matrix        
        H = (R_bar + np.dot(np.dot(S_bar.transpose(),Q_bar),S_bar))
        #print("H_matrix_first",H)
        #TO UNLOCK TO TRACK REF
        H += (np.dot(np.dot(S_bar_u.transpose(),W_delta_u_bar),S_bar_u)) 
        ###
        #print("H_matrix_second",(2*(np.dot(np.dot(S_bar_u.transpose(),W_delta_u_bar),S_bar_u))).shape)
        #print("H_matrix complete",H)
        #F matrix
        #print("##########")
        #print("T_bar",T_bar.shape)
        #print("Q_bar",Q_bar.shape)
        #print("S_bar",S_bar.shape)
        #print(len(self.init_states_list))
        #print(self.init_states_list[0].shape)
        #print(len(self.ref_value))
        u_0 = self.ref_value[0]
        x_0 = self.init_states_list[0]
        
        #da aggiungere uno stato in x_0
        #print("shapeee",(2*np.dot(np.dot(T_bar.transpose(),Q_bar),S_bar)).shape)
        #F_state_first = np.dot(x_0.transpose(),(2*np.dot(np.dot(T_bar.transpose(),Q_bar),S_bar)))
        F_state_first = np.dot(x_0.transpose(),np.dot(C,(2*np.dot(np.dot(T_bar.transpose(),Q_bar),S_bar))))
        F_input_first = np.dot(u_0.transpose(),(2*np.dot(np.dot(T_bar_u.transpose(),W_delta_u_bar),S_bar_u)))
        #print("ref_val",self.ref_value)
        u_ref = np.asarray(self.ref_value).reshape(self.N_step,1)
        #x_ref = np.asarray(self.init_states_list).reshape(self.state_dim*self.N_step,1)
        x_ref = np.asarray(self.init_states_list).reshape(2*self.N_step,1)
        F_state_second =  np.dot(x_ref.transpose(),(-2*(np.dot(Q_bar,S_bar))))
        F_input_second =  np.dot(u_ref.transpose(),(-2*(np.dot(W_delta_u_bar,S_bar_u))))
        #print("F_state_first",F_state_first.shape)
        #print("F_state_second",F_state_second.shape)
        #print("F_input_first",F_input_first.shape)
        #print("F_input_second",F_input_second.shape)
        #TO UNLOCK TO TRACK REF
        F_first_row = F_state_first + F_input_first
        F_second_row = F_state_second + F_input_second
        F = F_first_row + F_second_row
        ####
        #F_tra = np.concatenate((F_first_row,F_second_row),axis = 0)


        ##CONSTRAINT
        C_constr = np.zeros((1, 5))
        C_constr[0][4] = 1
        #print("C_constr",C_constr)
        S_bar_contr = np.zeros((self.N_step,1*self.N_step))
        T_bar_contr = np.zeros((self.N_step,self.state_dim))
        for i in range(0,self.N_step):
            for j in range(0,self.N_step):
                #A = self.A_list[i-j]
                #B = self.B_list[i-j]
                #print("riga",i)
                #print("colonna",j)
                if((j-i) < 0):
                    A = self.A_list[(j+1)]
                    #print("moltiplica A_index",j+1)
                    for z in range(j+2,(i+1)):
                        #print("per A_index",z)
                        A_temp = self.A_list[z]
                        A = np.dot(A_temp,A)             
                    B = self.B_list[j]
                    #print("AB",np.dot(LA.matrix_power(A,(i-j)),B))
                    #print("C",C)
                    #print("mul",np.dot(C,np.dot(LA.matrix_power(A,(i-j)),B)))
                    S_bar_contr[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr,np.dot(A,B))
                elif((j-i) == 0):
                    #print("entro solo per B!")
                    B = self.B_list[j]
                    S_bar_contr[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr,B)
                else:
                    S_bar_contr[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = 0.0
            A = self.A_list[0]
            for z in range(1,(i+1)):
                #print("entro",i)
                #print("e prendo",z)
                A_temp = self.A_list[z]
                A = np.dot(A_temp,A)   
            T_bar_contr[0 + 1*i:1 + 1*i,:] = np.dot(C_constr,A)
            #print("T_bar",T_bar_contr)
        #G_one = S_bar_contr
        #G_two = -S_bar_contr
        G_one = np.eye(self.N_step,self.N_step)
        G_two = -np.eye(self.N_step,self.N_step)
        G = np.concatenate((G_one,G_two),axis = 0)
        W_one =  np.ones((self.N_step,1))*9.0
        W_two =  -np.ones((self.N_step,1))*9.0
        W = np.concatenate((W_one,W_two),axis = 0) 
        print("W",W)
        print("G",G)

        #S = np.zeros((100,5))
        #S[:, 4] = 1.0
        #S[4][:] = 1.0
        S_one = -T_bar_contr
        S_two = T_bar_contr
        S = np.concatenate((S_one,S_two),axis = 0)
        #print("state",self.init_states_list_full[0])
        print("S",S)
        #print("S*state",np.dot(S,self.init_states_list_full[0]))
        #print("W + S*state",W + np.dot(S,self.init_states_list_full[0]))
        #QPOASES
        nWSR = np.array([300])
        options = Options()
        options.setToMPC()
        #options.numRefinementSteps = 10
        #options.enableFlippingBounds = BooleanType.TRUE
        #options.enableFarBounds = BooleanType.TRUE
        options.printLevel = PrintLevel.NONE
        qp = QProblem(self.N_step,self.N_step*2)
        qp.setOptions(options)
        #print("H",H.shape)
        #print("F",F[0].shape)
        #qp.init(H,F[0],None,None,None,None,None,nWSR)
        print("Init_state",self.init_states_list_full[0])
        qp.init(H,F[0],G,None,None,None,np.reshape((W + np.dot(S,self.init_states_list_full[0])),(1,self.N_step*2))[0],nWSR)
        xOpt = np.zeros(50)
        qp.getPrimalSolution(xOpt)               
        decisionVariables = xOpt[0]
        #if(decisionVariables > 9):
        #    decisionVariables = 9
        #if(decisionVariables < -9):
        #    decisionVariables = -9 
        #for k in range(0,6):
        #    decisionVariables[k] = xOpt[k]
        print("SOLUTION")
        print(xOpt)
        #print("xx",xOpt)
        return decisionVariables


        
