import numpy as np
from numpy.linalg import inv
import math
from scipy.linalg import block_diag
from numpy import linalg as LA
from qpoases import PyQProblem as QProblem
from qpoases import PyOptions as Options
from qpoases import PyPrintLevel as PrintLevel
from qpoases import PyBooleanType as BooleanType
from qpoases import *
from copy import deepcopy

class Mpc_RL(object):
    def __init__(self,mpc_step,dt , Q, Q_reg, R, W_u, P, P_reg):
        #double m1;   //link 1 mass
        #double m2;   //link 2 mass
        #double a1;   //link 1 length (distance from joint 1 axis and joint 2 axis
        #double a2;   //link 2 length
        #double l1;   //distance between link 1 center of mass and joint 1
        #double l2;   //distance between link 2 center of mass and joint 2
        #double Im;   //inertia moment of the motor arount its axis
        #double Il1;  //inertia moment of link 1 arount joint 1 axis
        #double Il2;  //inertia moment of link 2 arount joint 2 axis
        m1 = 0.193  #Kg   
        m2 = 0.073  #Kg
        a1 = 0.1483 #m 
        a2 = 0.1804 #m  
        l1 = 0.1032 #m
        l2 = 0.1065 #m
        Im = 4.74e-4
        Il1 = m1*a1*a1/12
        Il2 = m2*a2*a2/12
        #definition of the dynamic parameters
        self.A1 = Il1 + Il2 + Im + m1*l1*l1 + m2*(a1*a1 + l2*l2)
        self.A2 = Il2 + m2*l2*l2
        self.A3 = m2*a1*l2
        self.A4 = m1*l1 + m2*a1
        self.A5 = m2*l2

        #MPC utils
        self.N_step = mpc_step
        #self.N_step_constraint = mpc_step
        self.N_step_constraint = int(mpc_step/2)
        #self.N_step_constraint = 10
        #self.delta_mpc = 0.001
        self.delta_mpc = dt
        self.state_dim = 5

        self.state_ref = 4

        self.A_list = []
        self.B_list = []
        self.init_states_list_full = []
        self.init_states_list = []
        self.init_input = []
        self.ref_input = []
        self.ref_state = []
        self.reg_state = []
        self.alpha = []
        #self.activate_alpha = activate_Alpha
        #self.activate_reg = activate_Reg
        eq_state = [3.141,0,0,0]
        eq_input = [0,0]
        
        self.Q = Q
        self.Q_reg = Q_reg
        self.P_reg = P_reg
        self.P = P
        self.W_u = W_u
        self.R = R
        #self.A_eq = self.storeAmatrix(eq_state,eq_input,0)
        #self.B_eq = self.storeBmatrix(eq_state,eq_input,0)
    
    def storeAmatrix(self,state,inputs,init):
        q1 = state[0]
        q2 = state[1]
        #print("stat",state)
        #print("q1",q1)
        #print("q2",q2)
        tau = inputs[0]
        q1dot = state[2]
        q2dot = state[3]
        A = np.zeros((4, 4))
        ####TO CHANGE
        A[0][2] = 1
        A[1][3] = 1
        ###########
        #-(490*a3*a5*cos(q1 + 2*q2) - 981*a2*a4*cos(q1) + 490*a3*a5*cos(q1))/(100*(a3^2*cos(q2)^2 - a1*a2 + a2^2))
        A[2][0] = -(490*self.A3*self.A5*math.cos(q1 + 2*q2) - 981*self.A2*self.A4*math.cos(q1) + 490*self.A3*self.A5*math.cos(q1))/(100*(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)))
        #(a3*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - ((a3*cos(q2)*q1dot^2 + (49*a5*cos(q1 + q2))/5)*(a2 + a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (a2*(a3*cos(q2)*q2dot^2 + 2*a3*q1dot*cos(q2)*q2dot - (49*a5*cos(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (2*a2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2 - (2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5)*(a2 + a3*cos(q2)))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2
        A[2][1] = (self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - ((self.A3*math.cos(q2)*math.pow(q1dot,2) + (49*self.A5*math.cos(q1 + q2))/5)*(self.A2 + self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (self.A2*(self.A3*math.cos(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.cos(q2)*q2dot - (49*self.A5*math.cos(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (2*self.A2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2) - (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5)*(self.A2 + self.A3*math.cos(q2)))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2)
        #-(2*a3*sin(q2)*(a2*q1dot + a2*q2dot + a3*q1dot*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[2][2] = -(2*self.A3*math.sin(q2)*(self.A2*q1dot + self.A2*q2dot + self.A3*q1dot*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        #(2*a2*a3*sin(q2)*(q1dot + q2dot))/(a1*a2 + a3^2*sin(q2)^2 - a2^2 - a3^2)
        A[2][3] = (2*self.A2*self.A3*math.sin(q2)*(q1dot + q2dot))/(self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.sin(q2),2) - math.pow(self.A2,2) - math.pow(self.A3,2))
        #(49*a5*cos(q1 + q2)*(a1 + 2*a3*cos(q2)))/(5*(a3^2*cos(q2)^2 - a1*a2 + a2^2)) - ((a2 + a3*cos(q2))*((49*a5*cos(q1 + q2))/5 + (981*a4*cos(q1))/100))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][0] =(49*self.A5*math.cos(q1 + q2)*(self.A1 + 2*self.A3*math.cos(q2)))/(5*(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))) - ((self.A2 + self.A3*math.cos(q2))*((49*self.A5*math.cos(q1 + q2))/5 + (981*self.A4*math.cos(q1))/100))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        # ((a2 + a3*cos(q2))*(a3*cos(q2)*q2dot^2 + 2*a3*q1dot*cos(q2)*q2dot - (49*a5*cos(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) + ((a3*cos(q2)*q1dot^2 + (49*a5*cos(q1 + q2))/5)*(a1 + 2*a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (a3*sin(q2)*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) - (2*a3*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5))/(a3^2*cos(q2)^2 - a1*a2 + a2^2) + (2*a3^2*cos(q2)*sin(q2)*(a2 + a3*cos(q2))*(a3*sin(q2)*q2dot^2 + 2*a3*q1dot*sin(q2)*q2dot + tau - (49*a5*sin(q1 + q2))/5 - (981*a4*sin(q1))/100))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2 + (2*a3^2*cos(q2)*sin(q2)*(a3*sin(q2)*q1dot^2 + (49*a5*sin(q1 + q2))/5)*(a1 + 2*a3*cos(q2)))/(a2^2 - a1*a2 + a3^2*cos(q2)^2)^2
        A[3][1] = ((self.A2 + self.A3*math.cos(q2))*(self.A3*math.cos(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.cos(q2)*q2dot - (49*self.A5*math.cos(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) + ((self.A3*math.cos(q2)*math.pow(q1dot,2) + (49*self.A5*math.cos(q1 + q2))/5)*(self.A1 + 2*self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) - (2*self.A3*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)) + (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A2 + self.A3*math.cos(q2))*(self.A3*math.sin(q2)*math.pow(q2dot,2) + 2*self.A3*q1dot*math.sin(q2)*q2dot + tau - (49*self.A5*math.sin(q1 + q2))/5 - (981*self.A4*math.sin(q1))/100))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2) + (2*math.pow(self.A3,2)*math.cos(q2)*math.sin(q2)*(self.A3*math.sin(q2)*math.pow(q1dot,2) + (49*self.A5*math.sin(q1 + q2))/5)*(self.A1 + 2*self.A3*math.cos(q2)))/math.pow((math.pow(self.A2,2) - self.A1*self.A2 + math.pow(self.A3,2)*math.pow(math.cos(q2),2)),2)
        #(2*a3*sin(q2)*(a1*q1dot + a2*q2dot + 2*a3*q1dot*cos(q2) + a3*q2dot*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][2] = (2*self.A3*math.sin(q2)*(self.A1*q1dot + self.A2*q2dot + 2*self.A3*q1dot*math.cos(q2) + self.A3*q2dot*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        #(2*a3*sin(q2)*(q1dot + q2dot)*(a2 + a3*cos(q2)))/(a3^2*cos(q2)^2 - a1*a2 + a2^2)
        A[3][3] =(2*self.A3*math.sin(q2)*(q1dot + q2dot)*(self.A2 + self.A3*math.cos(q2)))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        #A = (np.eye(self.state_dim) + A)*self.delta_mpc
        #self.A_list.append(A)
        B = np.zeros((4, 1))
        B[2][0] = -self.A2/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        B[3][0] = (self.A2 + self.A3*math.cos(q2))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        A_extended = np.zeros((5, 5))
        #devo discretizzare solo A, non A_extended
        A_extended[0:4,0:4] = np.eye(4) + A*self.delta_mpc
        #print("A_",A_extended)
        A_extended[0:4,4:5] = B*self.delta_mpc
        #print("B___",B)
        A_extended[4,4] = 1
        #print("A________",A_extended)
        #A_extended = np.eye(self.state_dim) + (A_extended)*self.delta_mpc
        #print("A_Matrix",A_extended)
        if(init == 0):
            return A_extended
        self.A_list.append(A_extended)


    def storeBmatrix(self,state,inputs,init):
        q1 = state[0]
        q2 = state[1]
        tau = inputs[0]
        q1dot = state[2]
        q2dot = state[3]
        B = np.zeros((4, 1))
        B[2][0] = -self.A2/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        B[3][0] = (self.A2 + self.A3*math.cos(q2))/(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2))
        
        B_extended = np.ones((5, 1))
        B_extended[0:4,:] = B*self.delta_mpc
        #print("operazioni")
        #print("dividendo",(math.pow(self.A3,2)*math.pow(math.cos(q2),2) - self.A1*self.A2 + math.pow(self.A2,2)))
        #print("B_Matrix before",B_extended)
        #B_extended = B_extended*self.delta_mpc
        #print("B_Matrix",B_extended)
        if(init == 0):
            return B_extended
        self.B_list.append(B_extended)

    def storeState_Refvalue(self,state,tau):
        #print("Stateappe",state)
        #print("Stateappe",state[0])
        #print("Stateappe",state[1])
        #print("ref",reference)
        #new_s = np.asarray([state[0],state[1]])
        #new_s = np.asarray([state[0],state[1],state[2],state[3]])
        #print("new_s",new_s)

        #self.init_states_list.append(new_s)
        #self.init_states_list_full.append(state)
        #print("INIT_STATE",self.init_states_list_full)
        orig_state = np.asarray([state[0],state[1],state[2],state[3]])
        reg_state = np.asarray([[3.141,0,0,0]])
        self.ref_state.append(orig_state)
        self.ref_input.append(tau)
        self.reg_state.append(reg_state)

        #if(self.activate_alpha == 1):
        new_state = deepcopy(state[0])
        new_state_1 = deepcopy(state[1])
        if(state[0] < 0):
            new_state +=  2*math.pi
        #self.alpha.append(math.fabs(3.141 - new_state))
        self.alpha.append(math.fabs(math.fabs(3.141 - new_state) - math.fabs(0 - new_state_1)))
        #print("Alpha",math.fabs(3.141 - new_state/1))
        #else:
        #    self.alpha.append(0.5)
            #print("Alpha",1)
        #print("REF_Input",tau)
        #print("REF_state",orig_state)
        #print("REFERENCE",self.ref_value)

    def storeInitState(self,state,tau):
        #print("Stateappe",state)
        #print("Stateappe",state[0])
        #print("Stateappe",state[1])
        #print("ref",reference)
        #new_s = np.asarray([state[0],state[1]])
        #new_s = np.asarray([state[0],state[1],state[2],state[3]])
        #print("new_s",new_s)
        self.init_input.append(tau)
        self.init_states_list.append(state)
        #print("INIT_State",self.init_states_list)
        #print("INIT_Tau",self.init_input)

        full_state = np.concatenate((state,tau),axis = 0)
        self.init_states_list_full.append(full_state)

    
    def flush(self,):
        self.A_list = []
        self.B_list = []
        self.init_states_list_full = []
        self.init_states_list = []
        self.init_input = []
        self.ref_input = []
        self.ref_state = []
        self.reg_state = []
        self.alpha = []
    
    def compute_tau(self):
        #########moltiplicare A,B,C per T
        C = np.zeros((self.state_ref, 5))
        C[0][0] = 1
        C[1][1] = 1
        C[2][2] = 1
        C[3][3] = 1      
        #########separare calcolo A,B,C con questo
        S_bar = np.zeros((self.state_ref*self.N_step,1*self.N_step))
        T_bar = np.zeros((self.state_ref*self.N_step,self.state_dim))

        P = block_diag(self.P,self.P,self.P,self.P)
        Q = block_diag(self.Q,self.Q,self.Q,self.Q)*self.alpha[0]/6.28
        #R = 0.02*self.alpha[len(self.alpha) - 1]/6.28
        R = self.R
        
        Q_bar = Q
        R_bar = R  

        S_bar_reg = np.zeros((self.state_ref*self.N_step,1*self.N_step))
        T_bar_reg = np.zeros((self.state_ref*self.N_step,self.state_dim))

        P_reg = block_diag(self.P_reg,self.P_reg,self.P_reg,self.P_reg)
        Q_reg = block_diag(self.Q_reg,self.Q_reg,self.Q_reg,self.Q_reg)*(1 - (self.alpha[0]/6.28))
        R_reg = 0
        
        Q_bar_reg = Q_reg
        R_bar_reg = R_reg  

       
        for i in range(0,self.N_step):
            for j in range(0,self.N_step):
                if((j-i) < 0):
                    A = self.A_list[(j+1)]
                    for z in range(j+2,(i+1)):
                        A_temp = self.A_list[z]
                        A = np.dot(A_temp,A)             
                    B = self.B_list[j]
                    S_bar[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,np.dot(A,B))
                   # print("i",i)
                    #print("j",j)
                    S_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,np.dot(A,B))
                    #S_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,np.dot(LA.matrix_power(self.A_eq,(i-j)),self.B_eq))
                    ####
                elif((j-i) == 0):
                    #print("entro solo per B!")
                    B = self.B_list[j]
                    S_bar[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,B)
                    S_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,B)
                    #S_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = np.dot(C,self.B_eq)
                else:
                    S_bar[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = 0.0
                    S_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,0 + 1*j:1 + 1*j] = 0.0
            A = self.A_list[0]
            for z in range(1,(i+1)):
                A_temp = self.A_list[z]
                A = np.dot(A_temp,A)  
            T_bar[0 + self.state_ref*i:self.state_ref + self.state_ref*i,:] = np.dot(C,A)
            T_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,:] = np.dot(C,A)
            #T_bar_reg[0 + self.state_ref*i:self.state_ref + self.state_ref*i,:] = np.dot(C,LA.matrix_power(self.A_eq,(i+1)))
            
            #######################
            if(i == (self.N_step -1)):
                break
            R_bar = block_diag(R_bar, R)
            R_bar_reg = block_diag(R_bar_reg, R_reg)
            if(i < (self.N_step - 2)):
                Q_bar = block_diag(Q_bar, Q*self.alpha[i]/6.28)
                Q_bar_reg = block_diag(Q_bar_reg, Q_reg*(1-(self.alpha[i]/3.14)))
        
        #stato finale - forse una volta di troppo
        Q_bar = block_diag(Q_bar,P*self.alpha[i]/6.28)
        Q_bar_reg = block_diag(Q_bar_reg,P_reg*(1- (self.alpha[len(self.alpha) - 1]/6.28)))
        #print("PESI")
        #print("Q_bar",Q_bar)
        #print("Q_reg",Q_bar_reg)
        ###Calcolo per delta_u - delta_u_ref
        S_bar_u = np.zeros((1*self.N_step,1*self.N_step))
        T_bar_u = np.zeros((1*self.N_step,1))
        #W_u = 1.0
        W_delta_u = self.W_u*self.alpha[0]/6.28 
        #W_u_bar = W_u
        W_delta_u_bar = W_delta_u
        for i in range(0,self.N_step):
            for j in range(0,self.N_step):
                if((j-i) <= 0):
                    S_bar_u[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = 1
            T_bar_u[0 + 1*i:1 + 1*i,:] = 1
            if(i == (self.N_step -1)):
                break
            #W_u_bar = block_diag(W_u_bar, W_u)
            W_delta_u_bar = block_diag(W_delta_u_bar, W_delta_u*self.alpha[i])

        #H matrix        
        H = (R_bar + np.dot(np.dot(S_bar.transpose(),Q_bar),S_bar))

        H += (np.dot(np.dot(S_bar_reg.transpose(),Q_bar_reg),S_bar_reg))

        #TO UNLOCK TO TRACK REF
        H += (np.dot(np.dot(S_bar_u.transpose(),W_delta_u_bar),S_bar_u)) 

        #F matrix
        u_0 = self.init_input[0]
        x_0 = self.init_states_list[0]
        
        #F_state_first = np.dot(x_0.transpose(),(2*np.dot(np.dot(T_bar.transpose(),Q_bar),S_bar)))
        F_state_first = np.dot(x_0.transpose(),np.dot(C,(2*np.dot(np.dot(T_bar.transpose(),Q_bar),S_bar))))
        F_input_first = np.dot(u_0.transpose(),(2*np.dot(np.dot(T_bar_u.transpose(),W_delta_u_bar),S_bar_u)))
        F_reg_first = np.dot(x_0.transpose(),np.dot(C,(2*np.dot(np.dot(T_bar_reg.transpose(),Q_bar_reg),S_bar_reg))))

        u_ref = np.asarray(self.ref_input).reshape(self.N_step,1)
        #x_ref = np.asarray(self.init_states_list).reshape(self.state_dim*self.N_step,1)
        x_ref = np.asarray(self.ref_state).reshape(self.state_ref*self.N_step,1)
        x_reg = np.asarray(self.reg_state).reshape(self.state_ref*self.N_step,1)
        
        F_state_second =  np.dot(x_ref.transpose(),(-2*(np.dot(Q_bar,S_bar))))
        F_reg_second =  np.dot(x_reg.transpose(),(-2*(np.dot(Q_bar_reg,S_bar_reg))))
        F_input_second =  np.dot(u_ref.transpose(),(-2*(np.dot(W_delta_u_bar,S_bar_u))))

        #TO UNLOCK TO TRACK REF
        F_first_row = F_state_first + F_input_first + F_reg_first
        F_second_row = F_state_second + F_input_second + F_reg_second
        F = F_first_row + F_second_row

        ####
        #F_tra = np.concatenate((F_first_row,F_second_row),axis = 0)


        ##CONSTRAINT INPUT
        C_constr = np.zeros((1, 5))
        C_constr[0][4] = 1

        G_one = np.tril(np.ones(self.N_step_constraint))
        G_two = np.tril(np.ones(self.N_step_constraint))
        G = np.concatenate((G_one,G_two),axis = 0)
        #print("G",G)
        
        W_one =  np.ones((self.N_step_constraint,1))*9.0
        W_two =  -np.ones((self.N_step_constraint,1))*9.0
        W = np.concatenate((W_one,W_two),axis = 0) 


        S_one = -np.tile(C_constr,(self.N_step_constraint,1))
        S_two = -np.tile(C_constr,(self.N_step_constraint,1))
        S = np.concatenate((S_one,S_two),axis = 0)

        ######################
        ##CONSTRAINT OUTPUT
        '''C_constr_out = np.zeros((1, 5))
        C_constr_out[0][2] = 1
        #C_constr_out[1][3] = 1


        G_contr_out = np.zeros((1*self.N_step_constraint,1*self.N_step_constraint))
        S_contr_out = np.zeros((1*self.N_step_constraint,self.state_dim))
        
        for i in range(0,self.N_step_constraint):
            for j in range(0,self.N_step_constraint):
                if((j-i) < 0):
                    A = self.A_list[(j+1)]
                    for z in range(j+2,(i+1)):
                        A_temp = self.A_list[z]
                        A = np.dot(A_temp,A)             
                    B = self.B_list[j]
                    G_contr_out[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,np.dot(A,B))
                    #G_contr_out[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,np.dot(LA.matrix_power(self.A_eq,(i-j)),self.B_eq))
                    #G_contr_out[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,np.dot(A,B))
                elif((j-i) == 0):
                    #print("entro solo per B!")
                    B = self.B_list[j]
                    G_contr_out[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,B)
                    #G_contr_out[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,self.B_eq)
                    #G_contr_out[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = np.dot(C_constr_out,B)
                else:
                    #G_contr_out[0 + 1*i:1 + 1*i,0 + 1*j:1 + 1*j] = 0.0
                    G_contr_out[0 + 2*i:2 + 2*i,0 + 1*j:1 + 1*j] = 0.0
            A = self.A_list[0]
            for z in range(1,(i+1)):
                A_temp = self.A_list[z]
                A = np.dot(A_temp,A)   
            S_contr_out[0 + 1*i:1 + 1*i,:] = np.dot(C_constr_out,A)
            #S_contr_out[0 + 2*i:2 + 2*i,:] = np.dot(C_constr_out,A)
            #S_contr_out[0 + 1*i:1 + 1*i,:] = np.dot(C_constr_out,LA.matrix_power(self.A_eq,(i+1)))
        G_contr_State = np.concatenate((G_contr_out,G_contr_out),axis = 0)
        #print("g_contr",G_contr_State.shape)
        W_constr =  np.ones((1*self.N_step_constraint,1))*30000.0

        ############################################
        
        
        
        
        state_lb = np.reshape((-W_constr - np.dot(S_contr_out,self.init_states_list_full[0])),(1,1*self.N_step_constraint))[0]
        state_ub = np.reshape((W_constr - np.dot(S_contr_out,self.init_states_list_full[0])),(1,1*self.N_step_constraint))[0]
        #print("state_lb",state_lb)
        #print("state_ub",state_ub)
        #print("state_to_input_lb",np.dot(inv(G_contr_out),state_lb))
        #print("state_to_input_ub",np.dot(inv(G_contr_out),state_ub))'''

        input_lb = np.reshape((W_two + np.dot(S_two,self.init_states_list_full[0])),(1,self.N_step_constraint))[0]
        input_ub = np.reshape((W_one + np.dot(S_one,self.init_states_list_full[0])),(1,self.N_step_constraint))[0]
        #print("input_lb",input_lb)
        #print("input_ub",input_ub)
        #print("input_to_input_lb",np.dot(inv(G_one),input_lb))
        #print("input_to_input_ub",np.dot(inv(G_one),input_ub))

        #QPOASES
        nWSR = np.array([30000])
        options = Options()
        options.setToMPC()
        #options.enableFlippingBounds = BooleanType.TRUE
        #options.enableFarBounds = BooleanType.TRUE
        options.printLevel = PrintLevel.NONE
        #qp = QProblem(self.N_step,self.N_step*2)
        qp = QProblem(self.N_step,self.N_step_constraint)
        qp.setOptions(options)
        #print("G_input",G)
        #G_tot = np.concatenate((G,G_contr_State),axis = 0)
        #print("G_state",G_contr_State)
        #print("G_tot",G_tot)
        #lb = np.concatenate((input_lb,state_lb),axis = 0)
        #ub = np.concatenate((input_ub,state_ub),axis = 0)


        #qp.init(H,F[0],None,None,None,None,None,nWSR)
        #qp.init(H,F[0],G,None,None,np.reshape((W_two + np.dot(S_two,self.init_states_list_full[0])),(1,self.N_step))[0],np.reshape((W_one + np.dot(S_one,self.init_states_list_full[0])),(1,self.N_step))[0],nWSR)
        #qp.init(H,F[0],G,None,None,np.reshape((W_two + np.dot(S_two,self.init_states_list_full[0])),(1,self.N_step))[0],np.reshape((W_one + np.dot(S_one,self.init_states_list_full[0])),(1,self.N_step))[0],nWSR)
        qp.init(H,F[0],G,None,None,input_lb,input_ub,nWSR)
        #qp.init(H,F[0],G_contr_State,None,None,state_lb,state_ub,nWSR)
        #qp.init(H,F[0],G_tot,None,None,lb,ub,nWSR)
        xOpt = np.zeros(self.N_step)
        qp.getPrimalSolution(xOpt)               
        decisionVariables = xOpt[0]


        #print("SOLUTION")
        #print(xOpt)

        #a = np.dot(T_bar,self.init_states_list_full[0])
        #b = np.dot(S_bar,xOpt)

        #print("x_predict",(a.reshape(b.shape) + b).reshape(self.N_step,4))

        #print("x_reference",self.ref_state)
        return decisionVariables


        
