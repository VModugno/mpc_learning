#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 16:57:59 2018

@author: valerio
"""

# Copyright (c) 2015, Disney Research
# All rights reserved.
#
# Author(s): Sehoon Ha <sehoon.ha@disneyresearch.com>
# Disney Research Robotics Group
import pydart2 as pd
import numpy as np
import sys
from copy import deepcopy
sys.path.insert(0, './Env/Pendubot')
from Pendubot import *
sys.path.insert(0, './Ctrl')
from Mpc_RL import *
sys.path.insert(0, './Learn/ddpg')
from CriticNet import *
from ActorNet import *

from collections import deque
import random
import tensorflow as tf

#import matplotlib.pyplot as plt
import math
#import cma

#--------------
class OUNoise:
    def __init__(self, action_dimension: int, mu=0.0, theta=0.15, sigma=0.4, seed=120) -> None:
        self.action_dimension = action_dimension
        self.mu = mu
        self.theta = theta
        self.sigma = sigma
        self.state = np.ones(self.action_dimension) * self.mu
        self.reset()
        np.random.seed(seed)

    def reset(self) -> None:
        self.state = np.ones(self.action_dimension) * self.mu

    def noise(self) -> np.ndarray:
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.random.randn(len(x))
        self.state = x + dx
        return self.state
#-------------

class Learning(object):
    def __init__(self, skel, world, env):
        self.env = env
        self.world  = world
        self.mRobot = skel
        self.episode = 0
        self.steps = 0
        self.old_state = np.reshape([.0,.0,.0,.0],(4,1))
        self.old_action = np.reshape([.0],(1,1))
        self.reward = 0
        self.totalrew = 0
        self.done = 0
        
        self.num_episodes = 9000 #Numero episodi Train
        self.max_epLength = 400 #Lunghezza massima episodio
        self.REPLAY_MEMORY_SIZE = 100000 #dimensione replay_buffer
        self.batch_size = 128 #n. di esperienze per trainare - batch
        self.y = 0.99 #Discount factor
        self.state_dim = 4 #State dimension
        self.action_dim = 1 #Number action
        self.action_bound = [9.] #Bound action
        self.add_noise = OUNoise(self.action_dim) #Noise
        self.replay_memory = [] #Replay Memory


        self.plot_reward = [] #Plot Reward
        self.plot_episode = [] #Plot Num Episode
        
        #---------------
        self.session = tf.Session()
        self.Actor = Actor_Net(self.state_dim,self.action_dim,self.action_bound,self.batch_size,self.session)        
        self.Critic = Critic_Net(self.state_dim,self.action_dim,self.Actor.get_num_trainable_vars())
            
        self.to_inizialize_Actor = tf.variables_initializer([self.Actor.W_fc1, self.Actor.b_fc1, self.Actor.W_fc2, self.Actor.b_fc2,self.Actor.W_fc3, self.Actor.b_fc3])
        self.saver = tf.train.Saver([self.Actor.W_fc1, self.Actor.b_fc1, self.Actor.W_fc2, self.Actor.b_fc2,self.Actor.W_fc3, self.Actor.b_fc3])
        
        self.to_inizialize_Actor_Target = tf.variables_initializer([self.Actor.W_fc1_target, self.Actor.b_fc1_target, self.Actor.W_fc2_target, self.Actor.b_fc2_target,self.Actor.W_fc3_target, self.Actor.b_fc3_target])
        self.to_inizialize_Critic_Target = tf.variables_initializer([self.Critic.W_fc1_target, self.Critic.b_fc1_target, self.Critic.W_fc2_target, self.Critic.b_fc2_target,self.Critic.W_fc3_target, self.Critic.b_fc3_target,self.Critic.W_fc4_target,self.Critic.b_fc4_target])
        self.to_inizialize_Critic = tf.variables_initializer([self.Critic.W_fc1, self.Critic.b_fc1, self.Critic.W_fc2, self.Critic.b_fc2,self.Critic.W_fc3, self.Critic.b_fc3,self.Critic.W_fc4,self.Critic.b_fc4])

        self.session.run(tf.variables_initializer(var_list=tf.global_variables(scope='adam_Actor')))
        self.session.run(tf.variables_initializer(var_list=tf.global_variables(scope='adam_Critic')))
        
        self.session.run(self.to_inizialize_Actor_Target)
        self.session.run(self.to_inizialize_Critic)
        self.session.run(self.to_inizialize_Critic_Target)
        #----------------


    #def get_reward(self):
    #    reward = 0
    #    return reward

    def inference(self):
        #print("###INFERENCE###")
        state = self.env.get_state_true()
        state = state.squeeze().tolist()
        #print("state in inf",state)
        action = self.session.run(self.Actor.scaled_out, feed_dict={self.Actor.input: [state]})
        return action

    def inference_MPC(self):
        #print("###INFERENCE###")
        state = self.env.get_state_fake()
        state = state.squeeze().tolist()
        #print("state in inf",state)
        action = self.session.run(self.Actor.scaled_out, feed_dict={self.Actor.input: [state]})
        return action

    def train_step(self):
        total_steps = 0
        step_counts = []
        print("#######################")
        #To get State
        state = self.env.get_state()
        #appendo l'esperienza
        if(self.old_state is not None):   
            self.replay_memory.append((self.old_state,self.old_action/self.action_bound,self.env.get_reward(),state,False))
        self.old_state = state
        state = state.squeeze().tolist()
        print("State",state)
        ###calcolo azione
        #noise = self.add_noise.noise()*10
        #noise = noise/(1 +(self.episode/20))
        #noise = noise.reshape(1,self.action_dim)
        noise = (1. / (1. + (self.episode/200)))*9*np.random.randint(-1,2,(self.action_dim))
        print("Noise",noise) 
        #print("state_first",state)
        action = self.session.run(self.Actor.scaled_out, feed_dict={self.Actor.input: [state]})
        #action = action + noise
        action = action + noise
        #self.old_action = action
        action_to_act = action
        if(action_to_act[0][0] > 9):
            action_to_act[0][0] = 9
        if(action_to_act[0][0] < -9):
            action_to_act[0][0] = -9
        self.old_action = action_to_act
        print('Action',action_to_act)
        print("#######################")        
      
        #se buffer pieno, svuoto l'esperienza piu vecchia
        if len(self.replay_memory) > self.REPLAY_MEMORY_SIZE:
            self.replay_memory.pop(0)
        #se ho riempito abbastanza il buffer, posso iniziare la backprop
        
        if len(self.replay_memory) >= self.batch_size:
            minibatch = random.sample(self.replay_memory, self.batch_size)
            next_states = [m[3] for m in minibatch]
            next_states = np.reshape(next_states,(self.batch_size,self.state_dim))
            #dall'actor prendo l'azione da eseguire, nel bound dell'intervallo [-2,2]
            actor_non_scaled = self.session.run(self.Actor.out_fc3_target, feed_dict={self.Actor.input: next_states})
            #dal Critic, usando come input l'azione calcolata nell'actor, calcolo il Q-Values
            q_values = self.session.run(self.Critic.out_fc3_target  , feed_dict={self.Critic.input: next_states,self.Critic.actions: actor_non_scaled})
            #----------------
            target_q = np.zeros(self.batch_size)
            target_action_mask = np.zeros((self.batch_size, self.action_dim), dtype=float)
            for i in range(self.batch_size):
                _, action_sampled, old_reward, _, done_= minibatch[i]
                target_q[i] = old_reward
                if not done_:
                    target_q[i] += self.y * q_values[i][0]
                target_action_mask[i] = action_sampled

            target_q = np.reshape(target_q, (self.batch_size, 1))
            states = [m[0] for m in minibatch]
            states = np.reshape(states,(self.batch_size,self.state_dim))
            #traino il critic - NB uso gli elementi del replay buffer
            #print("States",states)
            #print("target_q",target_q)
            #print("action_mask",target_action_mask)
            _ = self.session.run(self.Critic.train_op, feed_dict={self.Critic.input: states,self.Critic.actions:target_action_mask,self.Critic.target:target_q})        

            #traino l'actor - NB uso gli elementi del replay buffer
            action_non_scaled = self.session.run(self.Actor.out_fc3, feed_dict={self.Actor.input: states})
            grads = self.session.run(self.Critic.action_grads, feed_dict={self.Critic.input: states,self.Critic.actions: action_non_scaled})
            _ = self.session.run(self.Actor.optimize, feed_dict={self.Actor.input: states,self.Actor.action_gradient: grads[0]})
         
            #updato il valore delle reti Target, poco alla volta, per evitare salti improvviso e destabilizazzione delle reti
            self.session.run(self.Actor.update_target_network_params)
            self.session.run(self.Critic.update_target_network_params)

        #dopo fine episodio - ciclo interno
        #self.totalrew += self.get_reward()

        if(self.steps % 70 == 0):
            self.reduction_noise = 0
            self.add_noise.reset()
        return action_to_act
        


class Controller(object):
       
    def initialize_constant(self):
        self.LEARN = False
        self.MPC_REINFORCED = True
        self.LEARN_TEST = False
    
    def initialize_conditions(self):
        self.initialize_constant()      
        self.qDotOld = np.zeros((2, 1)) 
        old_state = np.reshape([0.0,.0,.0,.0],(4,1)) 
        self.env.set_state(old_state)  
    
    def __init__(self, skel, world):
        self.world  = world
        self.mRobot = skel
        self.dt = 0.001
        old_state = np.reshape([0.0,.0,.0,.0],(4,1))
        #self.env = Pendubot(old_state,self.dt,0.193,0.073,0.1483,0.1804,0.1032,0.1065,4.74e-4)
        self.env = Pendubot(old_state,self.dt,0.350,0.273,0.1483,0.1804,0.1032,0.1065,4.74e-4)
        #self.env = Pendubot(old_state,self.dt)
        self.mpc_step = 8
        self.activate_alpha = 1
        self.activate_reg = 1
        self.mpc_rl = Mpc_RL(self.mpc_step,self.dt, self.activate_alpha,self.activate_reg)
        self.learning = Learning(self.mRobot,self.world,self.env)
        self.initialize_conditions()
        self.tau_old = 0
        
    
    def compute(self, ):
        #print("###COMPUTE###")
        state = self.env.get_state_true()
        #state_unmodified = self.env.get_state_unmodified()
        ###LEARN
        #print("State",state)
        #print("State_unmodified_inCompute",state_unmodified)
        if(self.LEARN):
            ####save
            if(self.learning.episode % 5 == 0):
                self.learning.saver.save(self.learning.session, 'saved_networks_ddpg/' + '-actor_critic')
                self.learning.episode += 1        
            ###resetto episodio se ultimo step##
            if(self.learning.steps % self.learning.max_epLength == 0 and self.learning.steps > 0):
                self.learning.replay_memory.append((self.learning.old_state,self.learning.old_action/self.learning.action_bound,self.learning.env.get_reward(),state,True))
                self.world.reset_train()
                self.initialize_conditions()
                self.learning.steps = 1
                self.learning.old_state = None
                tau  = np.zeros((1,1))
                self.learning.totalrew = 0
                self.learning.episode += 1
            else:                   
                tau = self.learning.train_step()
                self.learning.steps += 1
            tau = np.concatenate((tau,[[0.0]]),axis = 0)
        ###MPC_REINFORCED
        elif(self.LEARN_TEST):
            tau = self.learning.inference()
            tau = np.concatenate((tau,[[0.0]]),axis = 0)
            #print("Faccio",tau)
        else:
            tau = self.gen_Reference()
            tau += self.tau_old
            self.tau_old = tau
            #print("Faccio",tau)
            tau = np.concatenate(([[tau]],[[0.0]]),axis = 0)
        #ALL
        tau = np.reshape(tau,(2,1))
        self.env.step(tau)
        floatingBase  = np.zeros((6,1))
        q_new = self.env.get_state_true()[0:2,:]
        #q = np.concatenate((floatingBase,q_new),axis = 0)
        q =  np.reshape(q_new,(q_new.size))
        self.mRobot.set_positions(q)
        #print("#############")
        self.env.get_reward()
        #print("Reward",self.env.total_rew)
        #print("#############")

    def gen_Reference(self,):
        origin = deepcopy(self.env.get_state_true())
        #init_state = self.env.get_state_unmodified()
        init_state = deepcopy(self.env.get_state_true())
        if(init_state[0][0] < -2):
            init_state[0][0] +=  2*math.pi
        #if(init_state[1][0] > 0):
        #    init_state[1][0] -=  2*math.pi
        #print("State",init_state)
        new_state = deepcopy(init_state)
        tau = self.learning.inference_MPC()
        tau[0][0] = self.tau_old
        #self.env.set_state(origin)
        #new_state = np.concatenate((new_state,tau),axis = 0)
        self.mpc_rl.storeInitState(new_state,tau)
        for i in range(0,self.mpc_step):
            #print("qui0",self.env.get_state())
            tau = self.learning.inference_MPC()
            #if(i == 0):
            #print("Farei",tau)
            tau_complete = np.concatenate((tau,[[0.0]]),axis = 0)
            tau_complete = np.reshape(tau_complete,(2,1))
            self.env.step(tau_complete)
            #new_state = self.env.get_state_unmodified()
            new_state = deepcopy(self.env.get_state_fake())
            #print("qui1",self.env.get_state())

            ##LO STATO e' q(k),q(k)' e u(k-1)
            #print("storo",new_state)
            #print("Facendo",tau)
            #if(new_state[0][0] < 0):
                #print("DA",new_state[0][0])
                #new_state[0][0] = math.pi + (-new_state[0][0] + math.pi)
            #    new_state[0][0] +=  2*math.pi
                #print("A",new_state[0][0])
            self.mpc_rl.storeAmatrix(new_state,tau,1)
            self.mpc_rl.storeBmatrix(new_state,tau,1)
            if(new_state[0][0] < -2):
                #print("DA",new_state[0][0])
                #new_state[0][0] = math.pi + (-new_state[0][0] + math.pi)
                new_state[0][0] +=  2*math.pi
                #print("A",new_state[0][0])
            #if(init_state[1][0] > 0):
            #    init_state[1][0] -=  2*math.pi
            #print("qui2",self.env.get_state())    
            new_state = deepcopy(np.concatenate((new_state,tau),axis = 0))
            #print("storo",new_state)
            self.mpc_rl.storeState_Refvalue(new_state,tau) 
            ###############################
            #tau = self.learning.inference()
            #tau_complete = np.concatenate((tau,[[0.0]]),axis = 0)
            #tau_complete = np.reshape(tau_complete,(2,1))
            #self.env.step(tau_complete)
            #new_state = self.env.get_state()
            #print("qui3",self.env.get_state())
        #ripristino lo state
        tau = self.mpc_rl.compute_tau()
        if(tau > 9):
            tau = 9
        if(tau < -9):
            tau = -9

        #tau = 0
        self.mpc_rl.flush()
        self.env.set_state(origin)
        #tau = 0.0
        return tau





    
class MyWorld(pd.World):
    def __init__(self, ):
        pendubot_path = './Env/Pendubot/urdf/pendubot.urdf'
        #ground_path = './Env/Pendubot/urdf/ground.urdf'
        time_step=1.0/100.0
        pd.World.__init__(self,time_step)
        gravity = [0.0, 0.0 , 0]
        self.set_gravity(gravity)
        #self.add_skeleton(ground_path)
        self.add_skeleton(pendubot_path)

        # set joint in velocity mode
        pendubot = self.skeletons[0]
        sufficient_force = 100
        for joint in pendubot.joints:
            if(joint.get_num_dofs() == 1):
                print(joint)
                joint.set_actuator_type(2) # 4 stand for velocity
                joint.set_force_upper_limit(0,sufficient_force)
                joint.set_force_lower_limit(0,-sufficient_force)

        self.setInitialConfiguration()
        
    def reset_train(self):
        self.reset()
        self.setInitialConfiguration()
      
        
    def  setInitialConfiguration(self, ):
       #floatingBaseConfiguration  = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0],ndmin=2)
       joint  = np.array([0.0,0.0],ndmin=2)
       #q = np.concatenate((floatingBaseConfiguration,joint),axis = 1)
       q =  np.reshape(joint,(joint.size))
       print("si inizia")
       print(q.shape)
       self.skeletons[0].set_positions(q)
       self.skeletons[0].set_velocities(q)
       self.skeletons[0].set_forces(q)   


    def step(self, ):
        super(MyWorld, self).step()


if __name__ == '__main__':
    #Initialize Pydart
    pd.init()
    #Initialzie World
    world = MyWorld()
    #Initialize Controller
    skel = world.skeletons[0]
    skel.set_controller(Controller(skel, world))
    #Start Gui
    pd.gui.viewer.launch_pyqt5(world)
    
