#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 16:57:59 2018

@author: valerio
"""

# Copyright (c) 2015, Disney Research
# All rights reserved.
#
# Author(s): Sehoon Ha <sehoon.ha@disneyresearch.com>
# Disney Research Robotics Group
import numpy as np
import sys
from copy import deepcopy
sys.path.insert(0, './Env/Pendubot')
from Pendubot import *
sys.path.insert(0, './Ctrl')
from Mpc_RL_CMAES import *
sys.path.insert(0, './Learn/ddpg')
from CriticNet import *
from ActorNet import *

from collections import deque
import random
import tensorflow as tf

#import matplotlib.pyplot as plt
import math
import cma

#-------------

class Learning(object):
    def __init__(self, env):
        self.env = env
        self.episode = 0
        self.steps = 0
        self.old_state = np.reshape([.0,.0,.0,.0],(4,1))
        self.old_action = np.reshape([.0],(1,1))
        self.reward = 0
        self.totalrew = 0
        self.done = 0
        
        self.num_episodes = 9000 #Numero episodi Train
        self.max_epLength = 400 #Lunghezza massima episodio
        self.REPLAY_MEMORY_SIZE = 100000 #dimensione replay_buffer
        self.batch_size = 128 #n. di esperienze per trainare - batch
        self.y = 0.99 #Discount factor
        self.state_dim = 4 #State dimension
        self.action_dim = 1 #Number action
        self.action_bound = [9.] #Bound action
        #self.add_noise = OUNoise(self.action_dim) #Noise
        self.replay_memory = [] #Replay Memory


        self.plot_reward = [] #Plot Reward
        self.plot_episode = [] #Plot Num Episode
        
        #---------------
        self.session = tf.Session()
        self.Actor = Actor_Net(self.state_dim,self.action_dim,self.action_bound,self.batch_size,self.session)        
        self.Critic = Critic_Net(self.state_dim,self.action_dim,self.Actor.get_num_trainable_vars())
            
        self.to_inizialize_Actor = tf.variables_initializer([self.Actor.W_fc1, self.Actor.b_fc1, self.Actor.W_fc2, self.Actor.b_fc2,self.Actor.W_fc3, self.Actor.b_fc3])
        self.saver = tf.train.Saver([self.Actor.W_fc1, self.Actor.b_fc1, self.Actor.W_fc2, self.Actor.b_fc2,self.Actor.W_fc3, self.Actor.b_fc3])
        
        self.to_inizialize_Actor_Target = tf.variables_initializer([self.Actor.W_fc1_target, self.Actor.b_fc1_target, self.Actor.W_fc2_target, self.Actor.b_fc2_target,self.Actor.W_fc3_target, self.Actor.b_fc3_target])
        self.to_inizialize_Critic_Target = tf.variables_initializer([self.Critic.W_fc1_target, self.Critic.b_fc1_target, self.Critic.W_fc2_target, self.Critic.b_fc2_target,self.Critic.W_fc3_target, self.Critic.b_fc3_target,self.Critic.W_fc4_target,self.Critic.b_fc4_target])
        self.to_inizialize_Critic = tf.variables_initializer([self.Critic.W_fc1, self.Critic.b_fc1, self.Critic.W_fc2, self.Critic.b_fc2,self.Critic.W_fc3, self.Critic.b_fc3,self.Critic.W_fc4,self.Critic.b_fc4])

        self.session.run(tf.variables_initializer(var_list=tf.global_variables(scope='adam_Actor')))
        self.session.run(tf.variables_initializer(var_list=tf.global_variables(scope='adam_Critic')))
        
        self.session.run(self.to_inizialize_Actor_Target)
        self.session.run(self.to_inizialize_Critic)
        self.session.run(self.to_inizialize_Critic_Target)
        #----------------

    def inference(self):
        #print("###INFERENCE###")
        state = self.env.get_state_true()
        state = state.squeeze().tolist()
        #print("state in inf",state)
        action = self.session.run(self.Actor.scaled_out, feed_dict={self.Actor.input: [state]})
        return action

    def inference_MPC(self):
        #print("###INFERENCE###")
        state = self.env.get_state_fake()
        state = state.squeeze().tolist()
        #print("state in inf",state)
        action = self.session.run(self.Actor.scaled_out, feed_dict={self.Actor.input: [state]})
        return action


        


class Controller(object):
       
    def initialize_conditions(self):   
        self.qDotOld = np.zeros((2, 1)) 
        old_state = np.reshape([0.0,.0,.0,.0],(4,1)) 
        self.env.set_state(old_state)  
    
    def __init__(self,):
        self.dt = 0.001
        self.old_state = np.reshape([0.0,.0,.0,.0],(4,1))
        #self.env = Pendubot(old_state,self.dt,0.193,0.073,0.1483,0.1804,0.1032,0.1065,4.74e-4)
        self.env = Pendubot(self.old_state,self.dt,0.350,0.273,0.1483,0.1804,0.1032,0.1065,4.74e-4)
        #self.env = Pendubot(old_state,self.dt)
        
        
        self.mpc_step = 8
        self.activate_alpha = 1
        self.activate_reg = 1
        #self.mpc_rl = Mpc_RL(self.mpc_step,self.dt, self.activate_alpha,self.activate_reg)
        self.learning = Learning(self.env)
        self.initialize_conditions()
        self.tau_old = 0

    def setWeight(self,Q, Q_reg, R, W_u, P, P_reg):
        #print("Q",Q)
        #print("Q_reg",Q_reg)
        #print("R",R)
        #print("W_u",W_u)
        #print("P",P)
        #print("P_reg",P_reg)
        self.mpc_rl = Mpc_RL(self.mpc_step,self.dt, Q, Q_reg, R, W_u, P, P_reg)
        
    
    def compute(self, ):
        cumulative_error = 0
        for i in range (0,700):
            tau = self.gen_Reference()
            tau += self.tau_old
            self.tau_old = tau
            tau = np.concatenate(([[tau]],[[0.0]]),axis = 0)
            tau = np.reshape(tau,(2,1))
            actual = deepcopy(self.env.get_state_true())
            cumulative_error += math.fabs(math.fabs(3.141 - actual[0]) - math.fabs(0 - actual[1]))
            self.env.step(tau)
        #return self.env.fitness()
        self.env.set_state(self.old_state)
        print("Reward",-cumulative_error)
        self.tau_old = 0
        return (cumulative_error)
        


    def gen_Reference(self,):
        origin = deepcopy(self.env.get_state_true())
        init_state = deepcopy(self.env.get_state_true())
        if(init_state[0][0] < -2):
            init_state[0][0] +=  2*math.pi
        new_state = deepcopy(init_state)
        tau = self.learning.inference_MPC()
        tau[0][0] = self.tau_old
        self.mpc_rl.storeInitState(new_state,tau)
        for i in range(0,self.mpc_step):
            tau = self.learning.inference_MPC()
            tau_complete = np.concatenate((tau,[[0.0]]),axis = 0)
            tau_complete = np.reshape(tau_complete,(2,1))
            self.env.step(tau_complete)
            #new_state = self.env.get_state_unmodified()
            new_state = deepcopy(self.env.get_state_fake())
            self.mpc_rl.storeAmatrix(new_state,tau,1)
            self.mpc_rl.storeBmatrix(new_state,tau,1)
            if(new_state[0][0] < -2):
                new_state[0][0] +=  2*math.pi   
            new_state = deepcopy(np.concatenate((new_state,tau),axis = 0))
            self.mpc_rl.storeState_Refvalue(new_state,tau) 
        #ripristino lo state
        tau = self.mpc_rl.compute_tau()
        if(tau > 9):
            tau = 9
        if(tau < -9):
            tau = -9
        self.mpc_rl.flush()
        self.env.set_state(origin)
        return tau


def inner_loop(x,controller):
    print(x)
    controller.setWeight(x[0],x[1],x[2],x[3],x[4],x[5])
    return controller.compute()


if __name__ == '__main__':
    #Initialize Pydart
    #pd.init()
    #Initialzie World
    #world = MyWorld()
    #Initialize Controller
    #skel = world.skeletons[0]
    lower_CMA = [0,0,0,0,0,0]
    start_CMA = [100,0,0.1,1,100,100]
    controller = Controller()
    es = cma.CMAEvolutionStrategy(start_CMA, 1,{'bounds': [0, 150]})
    #for j in range(0,300):
    while not es.stop():
        X = es.ask()
        fit = []
        for i in range(0,len(X)):
            fit.append(inner_loop(X[i],controller))
        es.tell(X, fit)
        es.disp()
    #skel.set_controller(Controller(skel, world))
    #Start Gui
    #pd.gui.viewer.launch_pyqt5(world)
    
