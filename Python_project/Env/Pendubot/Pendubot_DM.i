
%module Pendubot_DM

%{
#define SWIG_FILE_WITH_INIT
#include <Python.h>
#include "Pendubot_DM.h"
%}

/* Include the NumPy typemaps library */
%include "numpy.i"

%init %{
  import_array();
%}

%include <std_vector.i>



%apply (float IN_ARRAY1[ANY]) {(float q[2])};
%apply (float IN_ARRAY1[ANY]) {(float dq[2])};
%apply (float IN_ARRAY1[ANY]) {(float ddq[2])};


%apply (float ARGOUT_ARRAY2[ANY][ANY]) {(float B[2][2])};
%apply (float ARGOUT_ARRAY2[ANY][ANY]) {(float B_dot[2][2])};
%apply (float ARGOUT_ARRAY1[ANY]) {(float C[2])};
%apply (float ARGOUT_ARRAY1[ANY]) {(float C_dot[2])};
%apply (float ARGOUT_ARRAY1[ANY]) {(float g[2])};
%apply (float ARGOUT_ARRAY1[ANY]) {(float g_dot[2])};
%apply (float ARGOUT_ARRAY1[ANY]) {(float tau[2])};



%include "Pendubot_DM.h"
