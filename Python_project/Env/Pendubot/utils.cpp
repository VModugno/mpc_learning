


#include "utils.h"

float** matrix_product(float **M1, float **M2, int row_m1, int col_m1, int col_m2)
{ int i,j,k;

  float** res = new float*[row_m1];
  for(i=0; i<row_m1; i++)
		res[i] = new float[col_m2];

  for(i=0; i<row_m1; i++) 
    { for(j=0; j<col_m2; j++)
	   { res[i][j] = 0;
		 for(k=0; k<col_m1; k++)
		 	res[i][j] += M1[i][k] * M2[k][j];
	   }
	}

  return res;
}



float* matrix_vector_product(float **M, float *v, int row_m, int col_m)
{ int i,j;
  float* res = new float[row_m];

  for(i=0; i<row_m; i++) 
    { res[i] = 0;
  	  for(j=0; j<col_m; j++)
		res[i] += M[i][j] * v[j];
	}

  return res;
}
