/* 
Pendubot_DM is a dynamic link library that implements a dynamic model for the Pendubot;
in particular, it offers the numerical computation of the mass matrix (2-by-2), the centrifugal and Coriolis vector (2-by-1)
the gravity vector (2-by-1) and the computation of the torques vector (2-by-1).

The dynamic model is B(q)*ddq + c(q,dq) + g(q) = tau
(frictions neglected)

The code has been written to generate a dynamic link library (in Windows, dll)

@author      Massimo Cefalo   <cefalo.m@gmail.com>
@contributor Valerio Modugno  <valerio.modugno@gmail.com>
@version 1.1
*/


#include <stdio.h>
#include <iostream>
#include <fstream>
using namespace std;


// This class is exported from the Pendubot_DM.dll
class Pendubot_DM {
public:

	// constructor
	Pendubot_DM(void);
	Pendubot_DM(double m1,double m2,double a1,double a2,double l1,double l2,double Im);

	// get_B returns the numerical computation of the current inertia matrix B(q)
	// inputs:
	//	-	float** B: a pointer to a 2-by-2 matrix, which will be filled with the numerical
	//		values of the inertia matrix B(q)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	// outputs: none
	void  get_B(float B[2][2],float q[2]) ;
	

	// get_B_dot returns the numerical computation of the first time derivative of the current inertia matrix B(q)
	// inputs:
	//	-	float** B_dot: a pointer to a 2-by-2 matrix, which will be filled with the numerical
	//		values of the inertia matrix B(q)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	//	-	float* q_dot: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint velocities in radians/sec
	// outputs: none
	void  get_B_dot(float B_dot[2][2],float q[2], float q_dot[2]);


	// get_c returns the numerical computation of the current Coriolis vector c(q,dq)
	// inputs:
	//	-	float* c: a pointer to a 2-by-1 vector, which will be filled with the numerical
	//		values of the Coriolis vector c(q,dq)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	//	-	float* dq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint velocities in radians/sec
	// outputs: none
	void get_c(float C[2], float q[2], float dq[2]);


	// get_c_dot returns the numerical computation of the first time derivative of the current Coriolis vector c(q,dq)
	// inputs:
	//	-	float* c: a pointer to a 2-by-1 vector, which will be filled with the numerical
	//		values of the Coriolis vector c(q,dq)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	//	-	float* dq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint velocities in radians/sec
	//	-	float* ddq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint accelerations in radians/sec
	// outputs: none
	void  get_c_dot(float c_dot[2], float q[2], float dq[2], float ddq[2]);


	// get_g returns the numerical computation of the current gravity vector g(q)
	// inputs:
	//	-	float* g: a pointer to a 2-by-1 vector, which will be filled with the numerical
	//		values of the gravity vector g(q)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	// outputs: none
	void  get_g(float g[2], float q[2]);
	


	// get_g_dot returns the numerical computation of the first time derivative of the current gravity vector g(q)
	// inputs:
	//	-	float* g: a pointer to a 2-by-1 vector, which will be filled with the numerical
	//		values of the gravity vector g(q)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	//	-	float* dq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint velocities in radians/sec
	// outputs: none
	void get_g_dot(float g_dot[2], float q[2], float dq[2]);



	// get_tau returns the numerical computation of the current torques vector tau(q,dq,ddq)
	// inputs:
	//	-	float* tau: a pointer to a 2-by-1 vector, which will be filled with the numerical
	//		values of the torques vector tau(q,dq,ddq)
	//	-	float* q: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint positions in radians
	//	-	float* dq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint velocities in radians/sec
	//	-	float* ddq: a pointer to a 2-by-1 vector which must contain the current values of the 
	//		joint accelerations in radians/(sec^2)
	// outputs: none
	void get_tau(float tau[2], float q[2], float dq[2], float ddq[2]);

private:
    //dynamic parameter of the Pendubot (for definition, see the class constructor and M.Cefalo Laurea thesis)
	double A1, A2, A3, A4, A5;
};
