from setuptools import find_packages
from setuptools import setup
from distutils.core import Extension 
from sys import platform as _platform
import sys
import glob
import os.path
import os
import numpy 


DIR = './'

CXX_FLAGS = '-Wall -msse2 -fPIC -std=c++11 '
CXX_FLAGS += '-O3 -DNDEBUG -shared '
CXX_FLAGS += '-g -fno-omit-frame-pointer -fno-inline-functions '
CXX_FLAGS += '-fno-optimize-sibling-calls '




cur_dir_path = os.path.dirname(os.path.realpath(__file__))

include_dirs = list()
include_dirs += ['/usr/include']

# Fetch python version
python_version = sys.version_info
python_major_version = python_version[0]
python_minor_version = python_version[1]

current_python = "python%d.%d" % (python_major_version, python_minor_version)
print("current_python = %s" % (current_python))
include_dirs += ['/usr/include/%s' % current_python]
include_dirs += ['/usr/local/include/%s' % current_python]
include_dirs += ['/usr/local/include']
include_dirs += ['/usr/local/include/eigen3']


try:
    import numpy
    NP_DIRS = [numpy.get_include()]
    print("numpy.get_include() = %s" % numpy.get_include())
except:
    NP_DIRS = list()
    NP_DIRS += ['/usr/local/lib/%s/dist-packages/numpy/core/include/' %
                current_python]
    NP_DIRS += ['/usr/lib/%s/dist-packages/numpy/core/include/' %
                current_python]
for d in NP_DIRS:
    print("numpy_include_dirs = %s" % d)
include_dirs += NP_DIRS

libraries = list()


if _platform == "linux" or _platform == "linux2":
    CXX_FLAGS += '-fno-inline-functions-called-once'

swig_opts = ['-c++']
if python_major_version == 3:
    swig_opts.append('-py3')
print("swig_opts: %s" % (str(swig_opts)))

sources = glob.glob(DIR + "/*.cpp")
sources.append(DIR + "Pendubot_DM.i")
sources = [f for f in sources if "wrap" not in f]  # Safe guard

print("source files:")
for source_file in sources:
    print("    > %s" % source_file)

print("depend files:")
depends = glob.glob("/*.h")
depends.append( DIR + 'numpy.i')
depends
for depend_file in depends:
    print("    > %s" % depend_file)



MANIFEST_in = "\n".join(["include %s" % f for f in depends])
print("Please check MANIFEST.in is:")
print("============================")
print(MANIFEST_in)
print("============================")


pendubot_DM        = Extension('_Pendubot_DM', 
                     sources=sources, 
                     include_dirs=include_dirs,
                     libraries=libraries,
                     extra_compile_args=CXX_FLAGS.split(),
                     swig_opts=swig_opts,
                     depends=depends) 


if isinstance(pendubot_DM, Extension):
    print("check")               
else:
    print("error")

requires = ['numpy',]


setup (name = 'Pendubot_DM', 
       version = '0.1', 
       author      = ' ', 
       description = ' ', 
       install_requires=requires,
       packages=find_packages(),
       ext_modules = [pendubot_DM]) 
