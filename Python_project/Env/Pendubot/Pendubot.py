import Pendubot_DM as pen
import numpy as np
from numpy.linalg import inv
import math
from copy import deepcopy

class Pendubot(object):
    
    def __init__(self, x_init,dt,*args, **kwargs):
        if len(args) >0:  
            foundOneArg = True  
        else:             
            foundOneArg = False 
        
        self.Pendubot_DM = None
        
        #"FAKE"
        self.state       = deepcopy(x_init)
        self.state_unmodified = deepcopy(x_init)
        
        #"TRUE"
        self.state_second       = deepcopy(x_init)
        self.state_second_unmodified = deepcopy(x_init)

        self.dt          = dt
        self.total_rew = 0
        self.Pendubot_DM = pen.Pendubot_DM()
        self.Pendubot_DM_modified = 0
        self.max_vel = 30
        if foundOneArg :
            self.Pendubot_DM_modified = pen.Pendubot_DM(*args)
        #else:
        #    self.Pendubot_DM = pen.Pendubot_DM()
            
    def dynamics(self,state,action):
        #print("ACTION_for_Dinamics",action)
        #print("Q_first",state[0:2])    
        q  = np.reshape(np.float32(state[0:2]),(2,)).tolist()
        qd = np.reshape(np.float32(state[2:4]),(2,)).tolist()
        #print("Q",q)
        B = self.Pendubot_DM.get_B(q)
        #print("B",B)
        B_inv = inv(B)
        #print("B_inv",B_inv)
        #print("B SHAPE",B.shape)
        c = np.reshape(self.Pendubot_DM.get_c(q,qd),(2,1))
        #print("C",c)
        #print("C SHAPE",c.shape)
        g = np.reshape(self.Pendubot_DM.get_g(q),(2,1))
        #print("G",g)
        #print("G SHAPE",g.shape)
        #print("left",-c - g + action)
        ddq = np.dot(B_inv,(-c - g + action))
        #print("SUM",-c - g + action)
        #print("#####")
        #print("DDQ",ddq)
        #print("DDQ SHAPE",ddq.shape)
        #print("#####")

        conc = np.concatenate(([[qd[0]]],[[qd[1]]],ddq),axis = 0)
        return conc

    def dynamics_secondModel(self,state,action):
        #print("ACTION_for_Dinamics",action)
        #print("Q_first",state[0:2])    
        q  = np.reshape(np.float32(state[0:2]),(2,)).tolist()
        qd = np.reshape(np.float32(state[2:4]),(2,)).tolist()
        #print("Q",q)
        B = self.Pendubot_DM_modified.get_B(q)
        #print("B",B)
        B_inv = inv(B)
        #print("B_inv",B_inv)
        #print("B SHAPE",B.shape)
        c = np.reshape(self.Pendubot_DM_modified.get_c(q,qd),(2,1))
        #print("C",c)
        #print("C SHAPE",c.shape)
        g = np.reshape(self.Pendubot_DM_modified.get_g(q),(2,1))
        #print("G",g)
        #print("G SHAPE",g.shape)
        #print("left",-c - g + action)
        ddq = np.dot(B_inv,(-c - g + action))
        #print("SUM",-c - g + action)
        #print("#####")
        #print("DDQ",ddq)
        #print("DDQ SHAPE",ddq.shape)
        #print("#####")

        conc = np.concatenate(([[qd[0]]],[[qd[1]]],ddq),axis = 0)
        return conc

    def get_state_true(self,):
        #return self.state
        return self.state_second
    def get_state_true_unmodified(self,):
        #return self.state_unmodified
        return self.state_second_unmodified
        
    def get_state_fake(self,):
        #return self.state_second
        return self.state
    def get_state_fake_unmodified(self,):
        #return self.state_second_unmodified
        return self.state_unmodified

    
    def set_state(self,state):
        self.state = state
        self.state_second = state

    def get_reward(self,): 
        #q  = np.reshape(np.float32(self.state[0:2]),(2,)).tolist()
        q  = np.reshape(np.float32(self.state),(4,)).tolist()
        #print("Q_0",q[0])
        reward = -math.sqrt(math.fabs(-math.fabs(math.fabs(q[0]) - 3.14159) - math.fabs(math.fabs(q[1]) - 0.0)))
        if(reward < 0.4 and reward > -0.4):
            reward = 0.1
        if(reward < 0.2 and reward > -0.2):
            reward = 0.3
        if(reward <= 0.15 and reward >= -0.15):
            reward = 0.5
        if(reward <= 0.1 and reward >= -0.1):
            reward = 0.7
        if(reward <= 0.05 and reward >= -0.05):
            reward = 1
        if(reward <= 0.02 and reward >= -0.02 and q[3] <= 0.1 and q[3] >= -0.1):
            reward = 3
        self.total_rew += reward
        #print("Reward",reward)
        return reward
    
    def step(self,tau):
        substeps = 1
        #piu step per infittire
        #runge kutta 4
        for i in range (0,substeps):
            k1                    = self.dynamics(self.state,tau)
            #print("K1",k1)
            #print("DIV",self.dt/2*k1)
            #print("STATE",self.state)
            #print("SUM",self.state+self.dt/2*k1)
            k2                    = self.dynamics(self.state+self.dt/2*k1,tau)
            k3                    = self.dynamics(self.state+self.dt/2*k2,tau)
            k4                    = self.dynamics(self.state+self.dt*k3,tau)
            new_state  =  self.state +  self.dt/6*(k1 + 2*k2 + 2*k3 + k4)
            #print("1",new_state)
            #print("BUU",new_state[0])
            #print("BUU",new_state[1])
            if(new_state[0][0] > math.pi):
                new_state[0][0] = -math.pi + (new_state[0][0] - math.pi)
            elif(new_state[0][0] < -math.pi):
                new_state[0][0] = math.pi - (-new_state[0][0] - math.pi)
            if(new_state[1][0] > math.pi):
                new_state[1][0] = -math.pi + (new_state[1][0] - math.pi)
            elif(new_state[1][0] < -math.pi):
                new_state[1][0] = math.pi - (-new_state[1][0] - math.pi)
            if(new_state[2][0] > self.max_vel):
                new_state[2][0] = self.max_vel
            elif(new_state[2][0] < -self.max_vel):
                new_state[2][0] = -self.max_vel
            if(new_state[3][0] > self.max_vel):
                new_state[3][0] = self.max_vel
            elif(new_state[3][0] < -self.max_vel):
                new_state[3][0] = -self.max_vel
            self.state = deepcopy(new_state)
            #print("State_modified_inStep_Fake",self.state)
        
        for i in range (0,substeps):
            k1                    = self.dynamics(self.state_unmodified,tau)
            #print("K1",k1)
            #print("DIV",self.dt/2*k1)
            #print("STATE",self.state)
            #print("SUM",self.state+self.dt/2*k1)
            k2                    = self.dynamics(self.state_unmodified+self.dt/2*k1,tau)
            k3                    = self.dynamics(self.state_unmodified+self.dt/2*k2,tau)
            k4                    = self.dynamics(self.state_unmodified+self.dt*k3,tau)
            new_state  =  self.state_unmodified +  self.dt/6*(k1 + 2*k2 + 2*k3 + k4)
            #print("state2",new_state)
            #print("BUU",new_state[0])
            #print("BUU",new_state[1])
            '''if(new_state[0][0] > math.pi):
                new_state[0][0] = -math.pi + (new_state[0][0] - math.pi)
            elif(new_state[0][0] < -math.pi):
                new_state[0][0] = math.pi - (-new_state[0][0] - math.pi)
            if(new_state[1][0] > math.pi):
                new_state[1][0] = -math.pi + (new_state[1][0] - math.pi)
            elif(new_state[1][0] < -math.pi):
                new_state[1][0] = math.pi - (-new_state[1][0] - math.pi)'''
            if(new_state[2][0] > self.max_vel):
                new_state[2][0] = self.max_vel
            elif(new_state[2][0] < -self.max_vel):
                new_state[2][0] = -self.max_vel
            if(new_state[3][0] > self.max_vel):
                new_state[3][0] = self.max_vel
            elif(new_state[3][0] < -self.max_vel):
                new_state[3][0] = -self.max_vel
            #self.state = new_state
        
            self.state_unmodified = deepcopy(new_state)
            #print("State_unmodified_inStep",self.state_unmodified)
        for i in range (0,substeps):
            k1                    = self.dynamics_secondModel(self.state_second,tau)
            #print("K1",k1)
            #print("DIV",self.dt/2*k1)
            #print("STATE",self.state)
            #print("SUM",self.state+self.dt/2*k1)
            k2                    = self.dynamics_secondModel(self.state_second+self.dt/2*k1,tau)
            k3                    = self.dynamics_secondModel(self.state_second+self.dt/2*k2,tau)
            k4                    = self.dynamics_secondModel(self.state_second+self.dt*k3,tau)
            new_state  =  self.state_second +  self.dt/6*(k1 + 2*k2 + 2*k3 + k4)
            #print("state3",new_state)
            #print("BUU",new_state[0])
            #print("BUU",new_state[1])
            if(new_state[0][0] > math.pi):
                new_state[0][0] = -math.pi + (new_state[0][0] - math.pi)
            elif(new_state[0][0] < -math.pi):
                new_state[0][0] = math.pi - (-new_state[0][0] - math.pi)
            if(new_state[1][0] > math.pi):
                new_state[1][0] = -math.pi + (new_state[1][0] - math.pi)
            elif(new_state[1][0] < -math.pi):
                new_state[1][0] = math.pi - (-new_state[1][0] - math.pi)
            if(new_state[2][0] > self.max_vel):
                new_state[2][0] = self.max_vel
            elif(new_state[2][0] < -self.max_vel):
                new_state[2][0] = -self.max_vel
            if(new_state[3][0] > self.max_vel):
                new_state[3][0] = self.max_vel
            elif(new_state[3][0] < -self.max_vel):
                new_state[3][0] = -self.max_vel
            self.state_second = deepcopy(new_state)
            #print("State_modified_inStep_True",self.state_second)
        
        #MODIFIED
        for i in range (0,substeps):
            k1                    = self.dynamics_secondModel(self.state_second_unmodified,tau)
            #print("K1",k1)
            #print("DIV",self.dt/2*k1)
            #print("STATE",self.state)
            #print("SUM",self.state+self.dt/2*k1)
            k2                    = self.dynamics_secondModel(self.state_second_unmodified+self.dt/2*k1,tau)
            k3                    = self.dynamics_secondModel(self.state_second_unmodified+self.dt/2*k2,tau)
            k4                    = self.dynamics_secondModel(self.state_second_unmodified+self.dt*k3,tau)
            new_state  =  self.state_second_unmodified +  self.dt/6*(k1 + 2*k2 + 2*k3 + k4)
            #print("state4",new_state)
            #print("BUU",new_state[0])
            #print("BUU",new_state[1])
            '''if(new_state[0][0] > math.pi):
                new_state[0][0] = -math.pi + (new_state[0][0] - math.pi)
            elif(new_state[0][0] < -math.pi):
                new_state[0][0] = math.pi - (-new_state[0][0] - math.pi)
            if(new_state[1][0] > math.pi):
                new_state[1][0] = -math.pi + (new_state[1][0] - math.pi)
            elif(new_state[1][0] < -math.pi):
                new_state[1][0] = math.pi - (-new_state[1][0] - math.pi)'''
            if(new_state[2][0] > self.max_vel):
                new_state[2][0] = self.max_vel
            elif(new_state[2][0] < -self.max_vel):
                new_state[2][0] = -self.max_vel
            if(new_state[3][0] > self.max_vel):
                new_state[3][0] = self.max_vel
            elif(new_state[3][0] < -self.max_vel):
                new_state[3][0] = -self.max_vel
            #self.state = new_state
        
            self.state_second_unmodified = deepcopy(new_state)
            #print("State_unmodified_inStep",self.state_unmodified)
            


###try
#state =  np.reshape([.0,.0,.0,.0],(4,1))
#dt = 0.05           
#pen = Pendubot(state,dt)
#action = np.reshape([.1,.0],(2,1))
#ddq = pen.dynamics(state,action)
#print("ddq",ddq)
            
                
                
                
               
            
        
    
        
        
        

       
        


 