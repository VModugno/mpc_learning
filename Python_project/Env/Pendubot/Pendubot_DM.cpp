// Pendubot_DM.cpp : Defines the exported functions for the DLL application.
//

#include "Pendubot_DM.h"
#include "utils.h"
#include <math.h>



// This is the constructor of a class that has been exported.
// see Pendubot_DM.h for the class definition
Pendubot_DM::Pendubot_DM()
{ double m1;   //link 1 mass
  double m2;   //link 2 mass
  double a1;   //link 1 length (distance from joint 1 axis and joint 2 axis
  double a2;   //link 2 length
  double l1;   //distance between link 1 center of mass and joint 1
  double l2;   //distance between link 2 center of mass and joint 2
  double Im;   //inertia moment of the motor arount its axis
  double Il1;  //inertia moment of link 1 arount joint 1 axis
  double Il2;  //inertia moment of link 2 arount joint 2 axis

  m1 = 0.193;  //Kg   
  m2 = 0.073;  //Kg
  a1 = 0.1483;  //m  (sulla tesi � 0.1492, ma in Kite � diverso)
  a2 = 0.1804;   //m  (sulla tesi � 0.1905, ma in Kite � diverso)
  l1 = 0.1032;  //m
  l2 = 0.1065;  //m
  Im = 4.74e-4;
  Il1 = m1*a1*a1/12;
  Il2 = m2*a2*a2/12;

  //definition of the dynamic parameters
  A1 = Il1 + Il2 + Im + m1*l1*l1 + m2*(a1*a1 + l2*l2);
  A2 = Il2 + m2*l2*l2;
  A3 = m2*a1*l2;
  A4 = m1*l1 + m2*a1;
  A5 = m2*l2;
}


Pendubot_DM::Pendubot_DM(double m1,double m2,double a1,double a2,double l1,double l2,double Im)
{
  double Il1;  //inertia moment of link 1 arount joint 1 axis
  double Il2;  //inertia moment of link 2 arount joint 2 axis
  Il1 = m1*a1*a1/12;
  Il2 = m2*a2*a2/12;

  //definition of the dynamic parameters
  A1 = Il1 + Il2 + Im + m1*l1*l1 + m2*(a1*a1 + l2*l2);
  A2 = Il2 + m2*l2*l2;
  A3 = m2*a1*l2;
  A4 = m1*l1 + m2*a1;
  A5 = m2*l2;
}




 void Pendubot_DM::get_B(float B[2][2],float q[2])
{ double q1, q2;

  q1 = (double)q[0];
  q2 = (double)q[1];

  B[0][0] = A1 + 2*A3*cos(q2);
  B[0][1] = A2 + A3*cos(q2);
  B[1][0] = A2 + A3*cos(q2);
  B[1][1] = A2;
}



 void Pendubot_DM::get_B_dot(float B_dot[2][2], float q[2], float dq[2])
{ double q1, q2, q1_dot, q2_dot;


  q1 = (double)q[0];
  q2 = (double)q[1];

  q1_dot = (double)dq[0];
  q2_dot = (double)dq[1];

  B_dot[0][0] = -2*A3*sin(q2)*q2_dot;
  B_dot[0][1] = -A3*sin(q2)*q2_dot;
  B_dot[1][0] = -A3*sin(q2)*q2_dot;
  B_dot[1][1] = 0;
}



void Pendubot_DM::get_c(float C[2],float q[2], float dq[2])
{ double q1, q2, q1dot, q2dot;


  q1 = (double)q[0];
  q2 = (double)q[1];

  q1dot = (double)dq[0];
  q2dot = (double)dq[1];

  C[0] = -A3*sin(q2)*q2dot*q1dot - A3*sin(q2)*(q1dot+q2dot)*q2dot;
  C[1] = A3*sin(q2)*q1dot*q1dot;
}




 void Pendubot_DM::get_c_dot(float C_dot[2], float q[2], float dq[2], float ddq[2])
{ double q1, q2, q1dot, q2dot, q1ddot, q2ddot;

  q1 = (double)q[0];
  q2 = (double)q[1];

  q1dot = (double)dq[0];
  q2dot = (double)dq[1];

  q1ddot = (double)ddq[0];
  q2ddot = (double)ddq[1];

  double sigma_1, sigma_2, sigma_3;

  //per i calcoli che seguono vedi il foglio di calcolo su MuPad
  sigma_1 = 2*q1dot + q2dot;
  sigma_2 = q2ddot;
  sigma_3 = q1ddot;

  C_dot[0] = -A3*cos(q2)*sigma_1*q2dot*q2dot - A3*sin(q2)*sigma_1*sigma_2 - A3*sin(q2)*(2*sigma_3+sigma_2)*q2dot; 
  C_dot[1] = A3*q1dot*(2*sin(q2)*sigma_3 + cos(q2)*q2dot*q1dot);
}



 void Pendubot_DM::get_g_dot(float g_dot[2], float q[2], float dq[2])
{ double q1, q2, q1dot, q2dot;
  double g_const = 9.80665;

  q1 = (double)q[0];
  q2 = (double)q[1];

  q1dot = (double)dq[0];
  q2dot = (double)dq[1];

  g_dot[0] = A4*g_const*cos(q1)*q1dot + A5*g_const*cos(q1+q2)*(q1dot+q2dot);
  g_dot[1] = A5*g_const*cos(q1+q2)*(q1dot+q2dot);
}



 void  Pendubot_DM::get_g(float g[2], float q[2])
{ double q1, q2;
  double g_const = 9.80665;

  q1 = (double)q[0];
  q2 = (double)q[1];

  g[0] = (double)(A4*g_const*sin(q1) + A5*g_const*sin(q1+q2));
  g[1] = (double)(A5*g_const*sin(q1+q2));
}




 void Pendubot_DM::get_tau(float tau[2], float q[2], float dq[2], float ddq[2])
{
  float *Bddq, *Sdq, *g;

  /*B = new float*[2];
  for(int i=0; i<2; i++)
		B[i] = new float[2];*/

  float B[2][2];

  Sdq = new float[2];
  g   = new float[2];

  this->get_B(B,q);
  this->get_c(Sdq,q,dq);
  this->get_g(g,q);

  Bddq = matrix_vector_product((float**)B,ddq,2,2);

  tau[0] = Bddq[0] + Sdq[0] + g[0];
  tau[1] = Bddq[1] + Sdq[1] + g[1];


  delete g;
  delete Sdq;
}




